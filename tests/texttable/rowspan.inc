<?php

/**
 * @file
 * Test case
 */

$cases = array();

$i = 0;

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'rows' => array(
    array(
      'data' => array(
        array(
          'rowspan' => 2,
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body b1',
        ),
        array(
          'rowspan' => 2,
          'data' => 'Body c1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'rowspan' => 2,
          'data' => 'Body b2',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a3',
        ),
        array(
          'data' => 'Body c3',
        ),
      ),
    ),
  ),
);

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'rows' => array(
    array(
      'data' => array(
        array(
          'rowspan' => 2,
          'data' => 'Body a',
        ),
        array(
          'data' => 'Body b',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body b',
        ),
      ),
    ),
  ),
);

$count = count($cases);
foreach (array_keys($cases) as $i) {
  $cases[$i]['message'] = t('Row span: @index', array('@index' => $i));

  $i_clone = sprintf('%02d', $i + $count);
  $cases[$i_clone] = $cases[$i];
  $cases[$i_clone]['message'] = t('Row span: @index', array('@index' => $i_clone));
  $cases[$i_clone]['options']['border'] = FALSE;
  $cases[$i_clone]['attributes'] = array(
    array('n' => 'padding_left',  'v' => 0, 's' => NULL),
    array('n' => 'padding_right', 'v' => 0, 's' => NULL),
  );
}

$i = 0;
$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┬─────────┬─────────┐
│ Body a1 │ Body b1 │ Body c1 │
│         ├─────────┤         │
│         │ Body b2 │         │
├─────────┤         ├─────────┤
│ Body a3 │         │ Body c3 │
└─────────┴─────────┴─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌────────┬────────┐
│ Body a │ Body b │
│        ├────────┤
│        │ Body b │
└────────┴────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
Body a1 Body b1 Body c1
        Body b2
Body a3         Body c3
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
Body a Body b
       Body b
TABLE;

