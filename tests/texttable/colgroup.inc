<?php

/**
 * @file
 * Colgroup tester
 */

$i = 0;
$cases = array();

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'options' => array(
    'border' => TRUE,
    'numOfHeaderRows' => 2,
    'numOfFooterRows' => 2,
    'maxWidth' => 120,
    'colgroup' => array(
      1 => array(
        'colspan' => 2,
        'padding_right' => 4,
      ),
      4 => array(
        'colspan' => 2,
        'padding_left' => 4,
        'padding_left_foot' => 1,
        'padding_right_foot' => 4,
      ),
    ),
  ),
  'attributes' => array(
    array('n' => 'align', 'v' => GitHookerTextTable::ALIGN_CENTER, 's' => 'head'),
    array('n' => 'align', 'v' => GitHookerTextTable::ALIGN_LEFT,   's' => 'body'),
    array('n' => 'align', 'v' => GitHookerTextTable::ALIGN_RIGHT,  's' => 'foot'),
  ),
  'rows' => array(
    array(
      'data' => array(
        array(
          'data' => 'header a',
        ),
        array(
          'data' => 'header b',
        ),
        array(
          'data' => 'header c',
        ),
        array(
          'data' => 'header d',
        ),
        array(
          'data' => 'header e',
        ),
        array(
          'data' => 'header f',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'long header a',
        ),
        array(
          'data' => 'long header b',
        ),
        array(
          'data' => 'long header c',
        ),
        array(
          'data' => 'long header d',
        ),
        array(
          'data' => 'long header e',
        ),
        array(
          'data' => 'long header f',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'body a',
        ),
        array(
          'data' => 'body b',
        ),
        array(
          'data' => 'body c',
        ),
        array(
          'data' => 'body d',
        ),
        array(
          'data' => 'body e',
        ),
        array(
          'data' => 'body f',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'long body a',
        ),
        array(
          'data' => 'long body b',
        ),
        array(
          'data' => 'long body c',
        ),
        array(
          'data' => 'long body d',
        ),
        array(
          'data' => 'long body e',
        ),
        array(
          'data' => 'long body f',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'footer a',
        ),
        array(
          'data' => 'footer b',
        ),
        array(
          'data' => 'footer c',
        ),
        array(
          'data' => 'footer d',
        ),
        array(
          'data' => 'footer e',
        ),
        array(
          'data' => 'footer f',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'long footer a',
        ),
        array(
          'data' => 'long footer b',
        ),
        array(
          'data' => 'long footer c',
        ),
        array(
          'data' => 'long footer d',
        ),
        array(
          'data' => 'long footer e',
        ),
        array(
          'data' => 'long footer f',
        ),
      ),
    ),
  ),
);


$count = count($cases);
foreach (array_keys($cases) as $i) {
  $cases[$i]['message'] = t('Column group: @index', array('@index' => $i));
}

$i = 0;
$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬──────────────────┬──────────────────┬───────────────┬──────────────────┬──────────────────┐
│   header a    │   header b       │   header c       │   header d    │      header e    │      header f    │
├───────────────┼──────────────────┼──────────────────┼───────────────┼──────────────────┼──────────────────┤
│ long header a │ long header b    │ long header c    │ long header d │    long header e │    long header f │
├───────────────┼──────────────────┼──────────────────┼───────────────┼──────────────────┼──────────────────┤
│ body a        │ body b           │ body c           │ body d        │    body e        │    body f        │
├───────────────┼──────────────────┼──────────────────┼───────────────┼──────────────────┼──────────────────┤
│ long body a   │ long body b      │ long body c      │ long body d   │    long body e   │    long body f   │
├───────────────┼──────────────────┼──────────────────┼───────────────┼──────────────────┼──────────────────┤
│      footer a │      footer b    │      footer c    │      footer d │      footer e    │      footer f    │
├───────────────┼──────────────────┼──────────────────┼───────────────┼──────────────────┼──────────────────┤
│ long footer a │ long footer b    │ long footer c    │ long footer d │ long footer e    │ long footer f    │
└───────────────┴──────────────────┴──────────────────┴───────────────┴──────────────────┴──────────────────┘
TABLE;
