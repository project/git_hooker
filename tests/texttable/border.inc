<?php

/**
 * @file
 * Test case
 */

$cases = array();

$i = 0;

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'rows' => array(
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
  ),
);

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'rows' => array(
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
  ),
);

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'rows' => array(
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
  ),
);

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'rows' => array(
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
  ),
);

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'rows' => array(
    array(
      'data' => array(
        array(
          'rowspan' => 2,
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
  ),
);

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'rows' => array(
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'rowspan' => 2,
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
  ),
);

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'rows' => array(
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'rowspan' => 3,
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
  ),
);

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'rows' => array(
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'rowspan' => 2,
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
  ),
);

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'rows' => array(
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'rowspan' => 2,
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
  ),
);

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'rows' => array(
    array(
      'data' => array(
        array(
          'colspan' => 2,
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'rowspan' => 2,
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
  ),
);

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'message' => t('Border @index', array('@index' => $i)),
  'rows' => array(
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'rowspan' => 2,
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'colspan' => 2,
          'data' => 'Body a1',
        ),
      ),
    ),
  ),
);

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'message' => t('Border @index', array('@index' => $i)),
  'rows' => array(
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'rowspan' => 2,
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'rowspan' => 2,
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'colspan' => 2,
          'data' => 'Body a1',
        ),
      ),
    ),
  ),
);

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'message' => t('Border @index', array('@index' => $i)),
  'rows' => array(
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'colspan' => 2,
          'data' => 'Body a1',
        ),
        array(
          'rowspan' => 2,
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'colspan' => 2,
          'data' => 'Body a1',
        ),
      ),
    ),
  ),
);

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'message' => t('Border @index', array('@index' => $i)),
  'rows' => array(
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'colspan' => 3,
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'colspan' => 2,
          'data' => 'Body a1',
        ),
        array(
          'colspan' => 1,
          'rowspan' => 2,
          'data' => 'Body a1',
        ),
        array(
          'colspan' => 2,
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'colspan' => 2,
          'data' => 'Body a1',
        ),
        array(
          'colspan' => 2,
          'data' => 'Body a1',
        ),
      ),
    ),
  ),
);


$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'message' => t('Border @index', array('@index' => $i)),
  'rows' => array(
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
        array(
          'rowspan' => 2,
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'rowspan' => 2,
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'rowspan' => 2,
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
  ),
);

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'message' => t('Border @index', array('@index' => $i)),
  'rows' => array(
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'rowspan' => 2,
          'data' => 'Body a1',
        ),
        array(
          'rowspan' => 2,
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
  ),
);

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'message' => t('Border @index', array('@index' => $i)),
  'rows' => array(
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
        array(
          'rowspan' => 3,
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'rowspan' => 2,
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
  ),
);

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'rows' => array(
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'rowspan' => 2,
          'data' => 'Body a1',
        ),
        array(
          'rowspan' => 3,
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
  ),
);

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'rows' => array(
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
        array(
          'rowspan' => 4,
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'rowspan' => 2,
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a1',
        ),
        array(
          'data' => 'Body a1',
        ),
      ),
    ),
  ),
);

$count = count($cases);
foreach (array_keys($cases) as $i) {
  $cases[$i]['message'] = t('Border: @index', array('@index' => $i));
}

$i = 0;
$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┐
│ Body a1 │
└─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┐
│ Body a1 │
├─────────┤
│ Body a1 │
└─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┬─────────┐
│ Body a1 │ Body a1 │
└─────────┴─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┬─────────┐
│ Body a1 │ Body a1 │
├─────────┼─────────┤
│ Body a1 │ Body a1 │
└─────────┴─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┬─────────┐
│ Body a1 │ Body a1 │
│         ├─────────┤
│         │ Body a1 │
└─────────┴─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┬─────────┐
│ Body a1 │ Body a1 │
├─────────┤         │
│ Body a1 │         │
└─────────┴─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┬─────────┐
│ Body a1 │ Body a1 │
├─────────┤         │
│ Body a1 │         │
├─────────┤         │
│ Body a1 │         │
└─────────┴─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┬─────────┐
│ Body a1 │ Body a1 │
├─────────┼─────────┤
│ Body a1 │ Body a1 │
├─────────┤         │
│ Body a1 │         │
└─────────┴─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┬─────────┐
│ Body a1 │ Body a1 │
├─────────┤         │
│ Body a1 │         │
├─────────┼─────────┤
│ Body a1 │ Body a1 │
└─────────┴─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────────┐
│ Body a1           │
├─────────┬─────────┤
│ Body a1 │ Body a1 │
├─────────┤         │
│ Body a1 │         │
└─────────┴─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┬─────────┐
│ Body a1 │ Body a1 │
├─────────┤         │
│ Body a1 │         │
├─────────┴─────────┤
│ Body a1           │
└───────────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┬─────────┬─────────┐
│ Body a1 │ Body a1 │ Body a1 │
├─────────┤         ├─────────┤
│ Body a1 │         │ Body a1 │
├─────────┴─────────┤         │
│ Body a1           │         │
└───────────────────┴─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┬─────────┬─────────┐
│ Body a1 │ Body a1 │ Body a1 │
├─────────┴─────────┼─────────┤
│ Body a1           │ Body a1 │
├───────────────────┤         │
│ Body a1           │         │
└───────────────────┴─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┬────────────────────┬─────────┐
│ Body a1 │ Body a1            │ Body a1 │
├─────────┴────┬─────────┬─────┴─────────┤
│ Body a1      │ Body a1 │ Body a1       │
├──────────────┤         ├───────────────┤
│ Body a1      │         │ Body a1       │
└──────────────┴─────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┬─────────┬─────────┐
│ Body a1 │ Body a1 │ Body a1 │
├─────────┼─────────┤         │
│ Body a1 │ Body a1 │         │
├─────────┤         ├─────────┤
│ Body a1 │         │ Body a1 │
├─────────┼─────────┤         │
│ Body a1 │ Body a1 │         │
└─────────┴─────────┴─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┬─────────┬─────────┐
│ Body a1 │ Body a1 │ Body a1 │
├─────────┼─────────┼─────────┤
│ Body a1 │ Body a1 │ Body a1 │
├─────────┤         │         │
│ Body a1 │         │         │
├─────────┼─────────┼─────────┤
│ Body a1 │ Body a1 │ Body a1 │
└─────────┴─────────┴─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┬─────────┬─────────┐
│ Body a1 │ Body a1 │ Body a1 │
├─────────┼─────────┤         │
│ Body a1 │ Body a1 │         │
├─────────┤         │         │
│ Body a1 │         │         │
├─────────┼─────────┼─────────┤
│ Body a1 │ Body a1 │ Body a1 │
└─────────┴─────────┴─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┬─────────┬─────────┐
│ Body a1 │ Body a1 │ Body a1 │
├─────────┼─────────┼─────────┤
│ Body a1 │ Body a1 │ Body a1 │
├─────────┤         │         │
│ Body a1 │         │         │
├─────────┼─────────┤         │
│ Body a1 │ Body a1 │         │
└─────────┴─────────┴─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┬─────────┬─────────┐
│ Body a1 │ Body a1 │ Body a1 │
├─────────┼─────────┤         │
│ Body a1 │ Body a1 │         │
├─────────┤         │         │
│ Body a1 │         │         │
├─────────┼─────────┤         │
│ Body a1 │ Body a1 │         │
└─────────┴─────────┴─────────┘
TABLE;
