<?php

/**
 * @file
 * Test case
 */

$cases = array();

$tmp = array(
  array('k' => 'l', 'n' => t('left'),   'v' => GitHookerTextTable::ALIGN_LEFT),
  array('k' => 'c', 'n' => t('center'), 'v' => GitHookerTextTable::ALIGN_CENTER),
  array('k' => 'r', 'n' => t('right'),  'v' => GitHookerTextTable::ALIGN_RIGHT),
);

$i = 0;
foreach ($tmp as $h) {
  foreach ($tmp as $b) {
    foreach ($tmp as $f) {
      $i = sprintf('%02d', ++$i);
      $cases[$i] = array(
        'options' => array(
          'border' => TRUE,
          'numOfHeaderRows' => 2,
          'numOfFooterRows' => 2,
        ),
        'attributes' => array(
          array('n' => 'align', 'v' => $h['v'], 's' => 'head'),
          array('n' => 'align', 'v' => $b['v'], 's' => 'body'),
          array('n' => 'align', 'v' => $f['v'], 's' => 'foot'),
        ),
        'rows' => array(
          array(
            'data' => array(
              array(
                'data' => 'header a',
              ),
              array(
                'data' => 'header b',
              ),
              array(
                'data' => 'header c',
              ),
            ),
          ),
          array(
            'data' => array(
              array(
                'data' => 'long header a',
              ),
              array(
                'data' => 'long header b',
              ),
              array(
                'data' => 'long header c',
              ),
            ),
          ),
          array(
            'data' => array(
              array(
                'data' => 'body a',
              ),
              array(
                'data' => 'body b',
              ),
              array(
                'data' => 'body c',
              ),
            ),
          ),
          array(
            'data' => array(
              array(
                'data' => 'long body a',
              ),
              array(
                'data' => 'long body b',
              ),
              array(
                'data' => 'long body c',
              ),
            ),
          ),
          array(
            'data' => array(
              array(
                'data' => 'footer a',
              ),
              array(
                'data' => 'footer b',
              ),
              array(
                'data' => 'footer c',
              ),
            ),
          ),
          array(
            'data' => array(
              array(
                'data' => 'long footer a',
              ),
              array(
                'data' => 'long footer b',
              ),
              array(
                'data' => 'long footer c',
              ),
            ),
          ),
        ),
      );
    }
  }
}

$count = count($cases);
foreach (array_keys($cases) as $i) {
  $cases[$i] += array('options' => array());
  $cases[$i]['message'] = t('Align: @index', array('@index' => $i));

  $i_clone = sprintf('%02d', $i + $count);
  $cases[$i_clone] = $cases[$i];
  $cases[$i_clone]['message'] = t('Align: @index', array('@index' => $i_clone));
  $cases[$i_clone]['options']['border'] = FALSE;
  $cases[$i_clone]['attributes'][] = array('n' => 'padding_left',  'v' => 0, 's' => NULL);
  $cases[$i_clone]['attributes'][] = array('n' => 'padding_right', 'v' => 0, 's' => NULL);
}

$i = 0;
$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│ header a      │ header b      │ header c      │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│ body a        │ body b        │ body c        │
├───────────────┼───────────────┼───────────────┤
│ long body a   │ long body b   │ long body c   │
├───────────────┼───────────────┼───────────────┤
│ footer a      │ footer b      │ footer c      │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│ header a      │ header b      │ header c      │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│ body a        │ body b        │ body c        │
├───────────────┼───────────────┼───────────────┤
│ long body a   │ long body b   │ long body c   │
├───────────────┼───────────────┼───────────────┤
│   footer a    │   footer b    │   footer c    │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│ header a      │ header b      │ header c      │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│ body a        │ body b        │ body c        │
├───────────────┼───────────────┼───────────────┤
│ long body a   │ long body b   │ long body c   │
├───────────────┼───────────────┼───────────────┤
│      footer a │      footer b │      footer c │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│ header a      │ header b      │ header c      │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│    body a     │    body b     │    body c     │
├───────────────┼───────────────┼───────────────┤
│  long body a  │  long body b  │  long body c  │
├───────────────┼───────────────┼───────────────┤
│ footer a      │ footer b      │ footer c      │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│ header a      │ header b      │ header c      │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│    body a     │    body b     │    body c     │
├───────────────┼───────────────┼───────────────┤
│  long body a  │  long body b  │  long body c  │
├───────────────┼───────────────┼───────────────┤
│   footer a    │   footer b    │   footer c    │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│ header a      │ header b      │ header c      │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│    body a     │    body b     │    body c     │
├───────────────┼───────────────┼───────────────┤
│  long body a  │  long body b  │  long body c  │
├───────────────┼───────────────┼───────────────┤
│      footer a │      footer b │      footer c │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│ header a      │ header b      │ header c      │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│        body a │        body b │        body c │
├───────────────┼───────────────┼───────────────┤
│   long body a │   long body b │   long body c │
├───────────────┼───────────────┼───────────────┤
│ footer a      │ footer b      │ footer c      │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│ header a      │ header b      │ header c      │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│        body a │        body b │        body c │
├───────────────┼───────────────┼───────────────┤
│   long body a │   long body b │   long body c │
├───────────────┼───────────────┼───────────────┤
│   footer a    │   footer b    │   footer c    │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│ header a      │ header b      │ header c      │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│        body a │        body b │        body c │
├───────────────┼───────────────┼───────────────┤
│   long body a │   long body b │   long body c │
├───────────────┼───────────────┼───────────────┤
│      footer a │      footer b │      footer c │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│   header a    │   header b    │   header c    │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│ body a        │ body b        │ body c        │
├───────────────┼───────────────┼───────────────┤
│ long body a   │ long body b   │ long body c   │
├───────────────┼───────────────┼───────────────┤
│ footer a      │ footer b      │ footer c      │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│   header a    │   header b    │   header c    │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│ body a        │ body b        │ body c        │
├───────────────┼───────────────┼───────────────┤
│ long body a   │ long body b   │ long body c   │
├───────────────┼───────────────┼───────────────┤
│   footer a    │   footer b    │   footer c    │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│   header a    │   header b    │   header c    │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│ body a        │ body b        │ body c        │
├───────────────┼───────────────┼───────────────┤
│ long body a   │ long body b   │ long body c   │
├───────────────┼───────────────┼───────────────┤
│      footer a │      footer b │      footer c │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│   header a    │   header b    │   header c    │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│    body a     │    body b     │    body c     │
├───────────────┼───────────────┼───────────────┤
│  long body a  │  long body b  │  long body c  │
├───────────────┼───────────────┼───────────────┤
│ footer a      │ footer b      │ footer c      │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│   header a    │   header b    │   header c    │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│    body a     │    body b     │    body c     │
├───────────────┼───────────────┼───────────────┤
│  long body a  │  long body b  │  long body c  │
├───────────────┼───────────────┼───────────────┤
│   footer a    │   footer b    │   footer c    │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│   header a    │   header b    │   header c    │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│    body a     │    body b     │    body c     │
├───────────────┼───────────────┼───────────────┤
│  long body a  │  long body b  │  long body c  │
├───────────────┼───────────────┼───────────────┤
│      footer a │      footer b │      footer c │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│   header a    │   header b    │   header c    │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│        body a │        body b │        body c │
├───────────────┼───────────────┼───────────────┤
│   long body a │   long body b │   long body c │
├───────────────┼───────────────┼───────────────┤
│ footer a      │ footer b      │ footer c      │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│   header a    │   header b    │   header c    │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│        body a │        body b │        body c │
├───────────────┼───────────────┼───────────────┤
│   long body a │   long body b │   long body c │
├───────────────┼───────────────┼───────────────┤
│   footer a    │   footer b    │   footer c    │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│   header a    │   header b    │   header c    │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│        body a │        body b │        body c │
├───────────────┼───────────────┼───────────────┤
│   long body a │   long body b │   long body c │
├───────────────┼───────────────┼───────────────┤
│      footer a │      footer b │      footer c │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│      header a │      header b │      header c │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│ body a        │ body b        │ body c        │
├───────────────┼───────────────┼───────────────┤
│ long body a   │ long body b   │ long body c   │
├───────────────┼───────────────┼───────────────┤
│ footer a      │ footer b      │ footer c      │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│      header a │      header b │      header c │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│ body a        │ body b        │ body c        │
├───────────────┼───────────────┼───────────────┤
│ long body a   │ long body b   │ long body c   │
├───────────────┼───────────────┼───────────────┤
│   footer a    │   footer b    │   footer c    │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│      header a │      header b │      header c │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│ body a        │ body b        │ body c        │
├───────────────┼───────────────┼───────────────┤
│ long body a   │ long body b   │ long body c   │
├───────────────┼───────────────┼───────────────┤
│      footer a │      footer b │      footer c │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│      header a │      header b │      header c │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│    body a     │    body b     │    body c     │
├───────────────┼───────────────┼───────────────┤
│  long body a  │  long body b  │  long body c  │
├───────────────┼───────────────┼───────────────┤
│ footer a      │ footer b      │ footer c      │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│      header a │      header b │      header c │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│    body a     │    body b     │    body c     │
├───────────────┼───────────────┼───────────────┤
│  long body a  │  long body b  │  long body c  │
├───────────────┼───────────────┼───────────────┤
│   footer a    │   footer b    │   footer c    │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│      header a │      header b │      header c │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│    body a     │    body b     │    body c     │
├───────────────┼───────────────┼───────────────┤
│  long body a  │  long body b  │  long body c  │
├───────────────┼───────────────┼───────────────┤
│      footer a │      footer b │      footer c │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│      header a │      header b │      header c │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│        body a │        body b │        body c │
├───────────────┼───────────────┼───────────────┤
│   long body a │   long body b │   long body c │
├───────────────┼───────────────┼───────────────┤
│ footer a      │ footer b      │ footer c      │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│      header a │      header b │      header c │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│        body a │        body b │        body c │
├───────────────┼───────────────┼───────────────┤
│   long body a │   long body b │   long body c │
├───────────────┼───────────────┼───────────────┤
│   footer a    │   footer b    │   footer c    │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌───────────────┬───────────────┬───────────────┐
│      header a │      header b │      header c │
├───────────────┼───────────────┼───────────────┤
│ long header a │ long header b │ long header c │
├───────────────┼───────────────┼───────────────┤
│        body a │        body b │        body c │
├───────────────┼───────────────┼───────────────┤
│   long body a │   long body b │   long body c │
├───────────────┼───────────────┼───────────────┤
│      footer a │      footer b │      footer c │
├───────────────┼───────────────┼───────────────┤
│ long footer a │ long footer b │ long footer c │
└───────────────┴───────────────┴───────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
header a      header b      header c
long header a long header b long header c
body a        body b        body c
long body a   long body b   long body c
footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
header a      header b      header c
long header a long header b long header c
body a        body b        body c
long body a   long body b   long body c
  footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
header a      header b      header c
long header a long header b long header c
body a        body b        body c
long body a   long body b   long body c
     footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
header a      header b      header c
long header a long header b long header c
   body a        body b        body c
 long body a   long body b   long body c
footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
header a      header b      header c
long header a long header b long header c
   body a        body b        body c
 long body a   long body b   long body c
  footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
header a      header b      header c
long header a long header b long header c
   body a        body b        body c
 long body a   long body b   long body c
     footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
header a      header b      header c
long header a long header b long header c
       body a        body b        body c
  long body a   long body b   long body c
footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
header a      header b      header c
long header a long header b long header c
       body a        body b        body c
  long body a   long body b   long body c
  footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
header a      header b      header c
long header a long header b long header c
       body a        body b        body c
  long body a   long body b   long body c
     footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
  header a      header b      header c
long header a long header b long header c
body a        body b        body c
long body a   long body b   long body c
footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
  header a      header b      header c
long header a long header b long header c
body a        body b        body c
long body a   long body b   long body c
  footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
  header a      header b      header c
long header a long header b long header c
body a        body b        body c
long body a   long body b   long body c
     footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
  header a      header b      header c
long header a long header b long header c
   body a        body b        body c
 long body a   long body b   long body c
footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
  header a      header b      header c
long header a long header b long header c
   body a        body b        body c
 long body a   long body b   long body c
  footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
  header a      header b      header c
long header a long header b long header c
   body a        body b        body c
 long body a   long body b   long body c
     footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
  header a      header b      header c
long header a long header b long header c
       body a        body b        body c
  long body a   long body b   long body c
footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
  header a      header b      header c
long header a long header b long header c
       body a        body b        body c
  long body a   long body b   long body c
  footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
  header a      header b      header c
long header a long header b long header c
       body a        body b        body c
  long body a   long body b   long body c
     footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
     header a      header b      header c
long header a long header b long header c
body a        body b        body c
long body a   long body b   long body c
footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
     header a      header b      header c
long header a long header b long header c
body a        body b        body c
long body a   long body b   long body c
  footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
     header a      header b      header c
long header a long header b long header c
body a        body b        body c
long body a   long body b   long body c
     footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
     header a      header b      header c
long header a long header b long header c
   body a        body b        body c
 long body a   long body b   long body c
footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
     header a      header b      header c
long header a long header b long header c
   body a        body b        body c
 long body a   long body b   long body c
  footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
     header a      header b      header c
long header a long header b long header c
   body a        body b        body c
 long body a   long body b   long body c
     footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
     header a      header b      header c
long header a long header b long header c
       body a        body b        body c
  long body a   long body b   long body c
footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
     header a      header b      header c
long header a long header b long header c
       body a        body b        body c
  long body a   long body b   long body c
  footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
     header a      header b      header c
long header a long header b long header c
       body a        body b        body c
  long body a   long body b   long body c
     footer a      footer b      footer c
long footer a long footer b long footer c
TABLE;
