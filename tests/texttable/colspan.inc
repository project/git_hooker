<?php

/**
 * @file
 * Test case
 */

$cases = array();
$i = 0;

$i = sprintf('%02d', ++$i);
$cases[$i] = array(
  'rows' => array(
    array(
      'data' => array(
        array(
          'data' => 'Body a',
        ),
        array(
          'data' => 'Body b',
        ),
        array(
          'data' => 'Body c',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'colspan' => 2,
          'data' => 'Body a',
        ),
        array(
          'data' => 'Body b',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a',
        ),
        array(
          'colspan' => 2,
          'data' => 'Body b',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a',
        ),
        array(
          'colspan' => 2,
          'align' => GitHookerTextTable::ALIGN_CENTER,
          'data' => 'Body b',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'data' => 'Body a',
        ),
        array(
          'colspan' => 2,
          'align' => GitHookerTextTable::ALIGN_RIGHT,
          'data' => 'Body b',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'colspan' => 3,
          'data' => 'Body a',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'colspan' => 3,
          'align' => GitHookerTextTable::ALIGN_CENTER,
          'data' => 'Body a',
        ),
      ),
    ),
    array(
      'data' => array(
        array(
          'colspan' => 3,
          'align' => GitHookerTextTable::ALIGN_RIGHT,
          'data' => 'Body a',
        ),
      ),
    ),
  ),
);

$count = count($cases);
foreach (array_keys($cases) as $i) {
  $cases[$i]['message'] = t('Column span: @index', array('@index' => $i));

  $i_clone = sprintf('%02d', $i + $count);
  $cases[$i_clone] = $cases[$i];
  $cases[$i_clone]['message'] = t('Column span: @index', array('@index' => $i_clone));
  $cases[$i_clone]['options']['border'] = FALSE;
  $cases[$i_clone]['attributes'] = array(
    array('n' => 'padding_left',  'v' => 0, 's' => NULL),
    array('n' => 'padding_right', 'v' => 0, 's' => NULL),
  );
}

$i = 0;
$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌────────┬────────┬────────┐
│ Body a │ Body b │ Body c │
├────────┴────────┼────────┤
│ Body a          │ Body b │
├────────┬────────┴────────┤
│ Body a │ Body b          │
├────────┼─────────────────┤
│ Body a │     Body b      │
├────────┼─────────────────┤
│ Body a │          Body b │
├────────┴─────────────────┤
│ Body a                   │
├──────────────────────────┤
│          Body a          │
├──────────────────────────┤
│                   Body a │
└──────────────────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
Body a Body b Body c
Body a        Body b
Body a Body b
Body a    Body b
Body a        Body b
Body a
       Body a
              Body a
TABLE;

