<?php

/**
 * @file
 * Test case
 */

$cases = array();

$i = 0;

$row_limit = 4;
$col_limit = 4;
for ($row = 0; $row < $row_limit; $row++) {
  for ($col = 0; $col < $col_limit; $col++) {
    $i = sprintf('%02d', ++$i);
    $cases[$i] = array();
    for ($r = 0; $r <= $row; $r++) {
      for ($c = 0; $c <= $col; $c++) {
        $cases[$i]['rows'][$r]['data'][$c]['data'] = trim(str_repeat('abc ', $r + 1));
      }
    }
  }
}

$count = count($cases);
foreach (array_keys($cases) as $i) {
  $cases[$i] += array('options' => array());
  $cases[$i]['message'] = t('Basic: @index', array('@index' => $i));

  $i_clone = sprintf('%02d', $i + $count);
  $cases[$i_clone] = $cases[$i];
  $cases[$i_clone]['message'] = t('Basic: @index', array('@index' => $i_clone));
  $cases[$i_clone]['options']['border'] = FALSE;
  $cases[$i_clone]['attributes'][] = array('n' => 'padding_left',  'v' => 0, 's' => NULL);
  $cases[$i_clone]['attributes'][] = array('n' => 'padding_right', 'v' => 0, 's' => NULL);
}

$i = 0;
$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────┐
│ abc │
└─────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────┬─────┐
│ abc │ abc │
└─────┴─────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────┬─────┬─────┐
│ abc │ abc │ abc │
└─────┴─────┴─────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────┬─────┬─────┬─────┐
│ abc │ abc │ abc │ abc │
└─────┴─────┴─────┴─────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┐
│ abc     │
├─────────┤
│ abc abc │
└─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┬─────────┐
│ abc     │ abc     │
├─────────┼─────────┤
│ abc abc │ abc abc │
└─────────┴─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┬─────────┬─────────┐
│ abc     │ abc     │ abc     │
├─────────┼─────────┼─────────┤
│ abc abc │ abc abc │ abc abc │
└─────────┴─────────┴─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────┬─────────┬─────────┬─────────┐
│ abc     │ abc     │ abc     │ abc     │
├─────────┼─────────┼─────────┼─────────┤
│ abc abc │ abc abc │ abc abc │ abc abc │
└─────────┴─────────┴─────────┴─────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────────┐
│ abc         │
├─────────────┤
│ abc abc     │
├─────────────┤
│ abc abc abc │
└─────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────────┬─────────────┐
│ abc         │ abc         │
├─────────────┼─────────────┤
│ abc abc     │ abc abc     │
├─────────────┼─────────────┤
│ abc abc abc │ abc abc abc │
└─────────────┴─────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────────┬─────────────┬─────────────┐
│ abc         │ abc         │ abc         │
├─────────────┼─────────────┼─────────────┤
│ abc abc     │ abc abc     │ abc abc     │
├─────────────┼─────────────┼─────────────┤
│ abc abc abc │ abc abc abc │ abc abc abc │
└─────────────┴─────────────┴─────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────────┬─────────────┬─────────────┬─────────────┐
│ abc         │ abc         │ abc         │ abc         │
├─────────────┼─────────────┼─────────────┼─────────────┤
│ abc abc     │ abc abc     │ abc abc     │ abc abc     │
├─────────────┼─────────────┼─────────────┼─────────────┤
│ abc abc abc │ abc abc abc │ abc abc abc │ abc abc abc │
└─────────────┴─────────────┴─────────────┴─────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────────────┐
│ abc             │
├─────────────────┤
│ abc abc         │
├─────────────────┤
│ abc abc abc     │
├─────────────────┤
│ abc abc abc abc │
└─────────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────────────┬─────────────────┐
│ abc             │ abc             │
├─────────────────┼─────────────────┤
│ abc abc         │ abc abc         │
├─────────────────┼─────────────────┤
│ abc abc abc     │ abc abc abc     │
├─────────────────┼─────────────────┤
│ abc abc abc abc │ abc abc abc abc │
└─────────────────┴─────────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────────────┬─────────────────┬─────────────────┐
│ abc             │ abc             │ abc             │
├─────────────────┼─────────────────┼─────────────────┤
│ abc abc         │ abc abc         │ abc abc         │
├─────────────────┼─────────────────┼─────────────────┤
│ abc abc abc     │ abc abc abc     │ abc abc abc     │
├─────────────────┼─────────────────┼─────────────────┤
│ abc abc abc abc │ abc abc abc abc │ abc abc abc abc │
└─────────────────┴─────────────────┴─────────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
┌─────────────────┬─────────────────┬─────────────────┬─────────────────┐
│ abc             │ abc             │ abc             │ abc             │
├─────────────────┼─────────────────┼─────────────────┼─────────────────┤
│ abc abc         │ abc abc         │ abc abc         │ abc abc         │
├─────────────────┼─────────────────┼─────────────────┼─────────────────┤
│ abc abc abc     │ abc abc abc     │ abc abc abc     │ abc abc abc     │
├─────────────────┼─────────────────┼─────────────────┼─────────────────┤
│ abc abc abc abc │ abc abc abc abc │ abc abc abc abc │ abc abc abc abc │
└─────────────────┴─────────────────┴─────────────────┴─────────────────┘
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
abc
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
abc abc
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
abc abc abc
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
abc abc abc abc
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
abc
abc abc
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
abc     abc
abc abc abc abc
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
abc     abc     abc
abc abc abc abc abc abc
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
abc     abc     abc     abc
abc abc abc abc abc abc abc abc
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
abc
abc abc
abc abc abc
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
abc         abc
abc abc     abc abc
abc abc abc abc abc abc
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
abc         abc         abc
abc abc     abc abc     abc abc
abc abc abc abc abc abc abc abc abc
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
abc         abc         abc         abc
abc abc     abc abc     abc abc     abc abc
abc abc abc abc abc abc abc abc abc abc abc abc
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
abc
abc abc
abc abc abc
abc abc abc abc
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
abc             abc
abc abc         abc abc
abc abc abc     abc abc abc
abc abc abc abc abc abc abc abc
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
abc             abc             abc
abc abc         abc abc         abc abc
abc abc abc     abc abc abc     abc abc abc
abc abc abc abc abc abc abc abc abc abc abc abc
TABLE;

$i = sprintf('%02d', ++$i);
$cases[$i]['expected'] = <<< TABLE
abc             abc             abc             abc
abc abc         abc abc         abc abc         abc abc
abc abc abc     abc abc abc     abc abc abc     abc abc abc
abc abc abc abc abc abc abc abc abc abc abc abc abc abc abc abc
TABLE;

