<?php

/**
 * @file
 * Token integration.
 */

/**
 * Implements hook_token_info().
 */
function git_hooker_token_info() {
  $return = array(
    'types' => array(
      'git_hooker_repository' => array(
        'name' => t('Git repository'),
        'description' => t('Git repository related tokens'),
        'needs-data' => 'git_hooker_repository',
      ),
    ),
    'tokens' => array(
      'git_hooker_repository' => array(
        'path' => array(
          'name' => t('Repository path'),
          'description' => t('Filesystem path to repository.'),
        ),
        'current-branch' => array(
          'name' => t('Current branch'),
          'description' => t('Current branch of the repository.'),
        ),
      ),
    ),
  );

  return $return;
}

/**
 * Implements hook_tokens().
 */
function git_hooker_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $return = array();
  if ($type == 'git_hooker' && !empty($data['git_hooker_repository'])) {
    $repo = $data['git_hooker_repository'];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'path':
          $return[$original] = $repo->path;
          break;

        case 'current-branch':
          $return[$original] = $repo->currentBranch;

      }
    }
  }
  return $return;
}
