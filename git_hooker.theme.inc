<?php

/**
 * @file
 * Theme layer functions
 */

/**
 * FormAPI git_hooker_regex_filter element themer.
 *
 * @ingroup themeable
 */
function theme_git_hooker_element_regex_filter($vars) {
  $table = array(
    'header' => array(),
    'rows' => array(),
    'empty' => t('No rows'),
    'attributes' => array(
      'id' => drupal_html_id('git-hooker-regex-filters-wrapper'),
      'class' => array('git-hooker-regex-filters-wrapper single'),
    ),
  );

  // Never incremented.
  $i = 0;
  foreach ($vars['element']['#gh_theme_cols'] as $col => $info) {
    $info += array('separator' => '<br />');
    $table['header'][$col] = $info['title'];

    $table['rows'][$i]['data'][$col] = array(
      'data' => array(),
      'class' => array(drupal_html_class($col)),
    );

    foreach ($info['fields'] as $key) {
      if (!isset($vars['element'][$key])) {
        continue;
      }

      $vars['element'][$key]['#title_display'] = 'none';

      if ($key == 'pattern') {
        $vars['element'][$key]['#attributes']['class'][] = 'monospace';
      }
      $table['rows'][$i]['data'][$col]['data'][] = render($vars['element'][$key]);
    }

    $table['rows'][$i]['data'][$col]['data'] = implode($info['separator'], $table['rows'][$i]['data'][$col]['data']);
  }

  return theme('table', $table);
}

/**
 * FormAPI git_hooker_regex_filters element themer.
 *
 * @ingroup themeable
 */
function theme_git_hooker_element_regex_filters($vars) {
  $table = array(
    'header' => array(),
    'rows' => array(),
    'empty' => t('No rows'),
    'attributes' => array(
      'id' => drupal_html_id('git-hooker-regex-filters-table'),
      'class' => array('git-hooker-regex-filters-wrapper multiple'),
    ),
  );

  foreach ($vars['element']['#gh_theme_cols'] as $col => $info) {
    $vars['element']['#gh_theme_cols'][$col] += array('separator' => '<br />');
    $table['header'][$col] = $info['title'];
  }

  $draggable_weight_class = 'draggable-weight';

  foreach (element_children($vars['element']) as $i) {
    if (empty($vars['element'][$i]['#type']) || $vars['element'][$i]['#type'] != 'git_hooker_regex_filter') {
      continue;
    }

    $table['rows'][$i] = array(
      'data' => array(),
      'class' => array('draggable'),
    );
    foreach ($vars['element']['#gh_theme_cols'] as $col => $info) {
      $table['rows'][$i]['data'][$col] = array(
        'data' => array(),
        'class' => array(drupal_html_class($col)),
      );

      foreach ($info['fields'] as $key) {
        if (!isset($vars['element'][$i][$key])) {
          continue;
        }

        $vars['element'][$i][$key]['#title_display'] = 'none';

        if ($key == 'pattern') {
          $vars['element'][$key]['#attributes']['class'][] = 'monospace';
        }

        if ($key == 'weight') {
          $vars['element'][$i][$key]['#attributes']['class'][] = $draggable_weight_class;
        }

        $table['rows'][$i]['data'][$col]['data'][$key] = render($vars['element'][$i][$key]);
      }
      $table['rows'][$i]['data'][$col]['data'] = implode($info['separator'], $table['rows'][$i]['data'][$col]['data']);
    }
  }

  if (isset($vars['element']['add_submit'])) {
    $table['rows']['add'] = array(
      'data' => array(),
      'class' => array('new-item'),
    );

    foreach ($vars['element']['#gh_theme_cols'] as $col => $info) {
      switch ($col) {
        case 'weight':
          if (isset($vars['element']['add_weight'])) {
            $table['rows']['add']['class'][] = 'draggable';

            $vars['element']['add_weight']['#title_display'] = 'none';
            $vars['element']['add_weight']['#attributes']['class'][] = $draggable_weight_class;
            $table['rows']['add']['data'][$col] = array(
              'data' => render($vars['element']['add_weight']),
              'class' => array($col),
            );
          }
          else {
            $table['rows']['add']['data'][$col] = array(
              'data' => '&nbsp;',
              'class' => array($col),
            );
          }
          break;

        case 'operation':
          $table['rows']['add']['data'][$col] = array(
            'data' => render($vars['element']['add_submit']),
            'class' => array($col),
          );
          break;

        default:
          $table['rows']['add']['data'][$col] = array(
            'data' => '&nbsp;',
            'class' => array($col),
          );
          break;
      }
    }
  }

  drupal_add_tabledrag($table['attributes']['id'], 'order', 'sibling', $draggable_weight_class);
  $return = '';
  $return .= '<div id="' . $vars['element']['#wrapper_id'] . '">';
  $return .= theme('table', $table);
  $return .= '</div>';
  return $return;
}
