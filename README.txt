
IMPORTANT

This module is a tool _only_ for developers who are using Git,
not for standard websites.


REQUIREMENTS

drush (Tested with drush 5.2)


INSTALLATION

1.  Create a new Drupal 7 based website on your development environment.
    In this example: http://desktop.localhost

2.  Enable this (git_hooker) module with dependencies.

3.  Configure an drush alias for the new drupal site.
    Edit your ~/.drush/aliases.drushrc.php file, and add the folowing contents to it:
    /* BEGIN CONTENT */
    $aliases['desktop'] = array(
      'root' => "{$_SERVER['HOME']}/path/to/new/drupal/root",
      'uri' => 'desktop.localhost',
    );
    /* END CONTENT */

4.  Edit configuration of your repositories.
    Small modification is required in each .git/hooks/* scripts
    to trigger the Drupal actions.

    4.1 pre-commit
        At the end of file the last command is:
        /* BEGIN CONTENT */
        # If there are whitespace errors, print the offending file names and fail.
        exec git diff-index --check --cached $against --
        /* END CONTENT */

        Above the quoted command, you need add the following content:
        /* BEGIN CONTENT */
        drush @desktop git-hooker-invoke pre-commit `pwd`
        if [[ $? -ne 0 ]]; then
          exit 1
        fi
        /* END CONTENT */

        After the modification the end of .git/pre-commit file looks like this:
        /* BEGIN CONTENT */
        drush @desktop git-hooker-invoke pre-commit `pwd`
        if [[ $? -ne 0 ]]; then
          exit 1
        fi

        # If there are whitespace errors, print the offending file names and fail.
        exec git diff-index --check --cached $against --
        /* END CONTENT */

5.  If you don't want edit each .git/hooks file in the future then read the Git manual
    git init --template=<dir>
    git clone --template=<dir>
    Create a template directory and configure it as default template directory.

6.  Go to http://desktop.localhost/admin/config/workflow/rules
    and create rules, choice an event from "Git hooks" group.

7.  For Drupalers
    Enable the module "Git Hooker - Feature Drupal" (ghf_drupal)

IMPORTANT
Any ideas will be appreciated.
