<?php

/**
 * @file
 * Rules module integration.
 *
 * @addtogroup rules
 */

/**
 * Implements hook_rules_data_info().
 */
function git_hooker_rules_data_info() {
  return git_hooker_pick_rules_items('data', 'git_hooker');
}

/**
 * Implements hook_rules_event_info().
 */
function git_hooker_rules_event_info() {
  return git_hooker_pick_rules_items('event', 'git_hooker');
}

/**
 * Implements hook_rules_action_info().
 */
function git_hooker_rules_action_info() {
  return git_hooker_pick_rules_items('action', 'git_hooker');
}

/**
 * Implements hook_rules_file_info().
 */
function git_hooker_rules_file_info() {
  return git_hooker_pick_rules_files('git_hooker');
}

/**
 * Simple text input element for "text" data type.
 */
class RulesDataUIGitHookerTextfield extends RulesDataUIText {
  public static function inputForm($name, $info, $settings, RulesPlugin $element) {
    $form = parent::inputForm($name, $info, $settings, $element);
    if ($form[$name]['#type'] == 'textarea') {
      unset($form[$name]['#rows']);
      $form[$name]['#type'] = 'textfield';
    }
    return $form;
  }
}

class RulesDataUIGitHookerSelect extends RulesDataUIText {
  public static function inputForm($name, $info, $settings, RulesPlugin $element) {
    $form = parent::inputForm($name, $info, $settings, $element);
    if ($form[$name]['#type'] == 'select' && !empty($form[$name]['#options'])) {
      $form[$name]['#type'] = ((count($form[$name]['#options']) > 4 || array_filter($form[$name]['#options'], 'is_array')) ? 'select' : 'radios');
    }
    return $form;
  }
}
