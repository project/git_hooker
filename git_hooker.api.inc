<?php

/**
 * @file
 * API documentation
 */

function hook_git_hooker_filetype_info() {
  return array(
    'php' => array(
      // Required
      'label' => 'PHP',

      // Required
      'group' => t('Scripts'),

      // Required (can be empty array)
      'mimetype' => array('text/x-php4-src', 'text/x-php3-src', 'text/vnd.wap.wml', 'application/x-php'),

      // Required with least one item.
      'filters' => array(
        array('weight' => 0, 'invert' => FALSE, 'exclude' => FALSE, 'pattern' => '@(/|^).*\.php$@'),
        array('weight' => 1, 'invert' => FALSE, 'exclude' => FALSE, 'pattern' => '@(/|^).*\.php3$@'),
        array('weight' => 2, 'invert' => FALSE, 'exclude' => FALSE, 'pattern' => '@(/|^).*\.wml$@'),
        array('weight' => 3, 'invert' => FALSE, 'exclude' => FALSE, 'pattern' => '@(/|^).*\.phtml$@'),
        array('weight' => 4, 'invert' => FALSE, 'exclude' => FALSE, 'pattern' => '@(/|^).*\.phtm$@'),
        array('weight' => 5, 'invert' => FALSE, 'exclude' => FALSE, 'pattern' => '@(/|^).*\.inc$@'),
      ),
    ),
  );
}
