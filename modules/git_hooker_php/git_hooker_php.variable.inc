<?php

/**
 * @file
 * Variable module integration
 */

/**
 * Implements hook_variable_info().
 */
function git_hooker_php_variable_info($options) {
  $return = array();

  $key = 'git_hooker_php_executables';
  $return[$key] = array(
    'type' => 'array',
    'group' => 'git_hooker_php',
    'title' => t('PHP executables', array(), $options),
    'description' => t('Description of this variable', array(), $options),
    'required' => FALSE,
    'default' => array(),
    'format callback' => 'git_hooker_php_var_executables_format',
    'element callback' => 'git_hooker_php_var_executables_element',
  );

  if (is_executable('/usr/bin/php-cgi')) {
    $return[$key]['default'][0] = '/usr/bin/php-cgi';
  }
  elseif (is_executable('/usr/bin/php-cli')) {
    $return[$key]['default'][0] = '/usr/bin/php-cli';
  }
  elseif (is_executable('/usr/bin/php')) {
    $return[$key]['default'][0] = '/usr/bin/php';
  }

  return $return;
}

/**
 * Implements hook_variable_group_info().
 */
function git_hooker_php_variable_group_info() {
  $return = array();

  $return['git_hooker_php'] = array(
    'title' => t('Git Hooker - PHP'),
    'description' => t('Desciption of this group'),
    'access' => 'git_hooker_php_administer',
    'path' => array(
      'admin/config/workflow/git-hooker/php',
    ),
  );

  return $return;
}

function git_hooker_php_var_executables_element($variable, $options = array()) {
  // This will be possibly a fieldset with tree value
  $element = variable_form_element_default($variable, $options);
  // We may have a multiple element base that will default to plain textfield
  $item = $variable['repeat'];
  $value = variable_get_value($variable, $options);
  // Compile names and defaults for all elements
  $names = $defaults = array();
  if (!empty($variable['multiple'])) {
    // If we've got a multiple value, it will be an array with known elements
    $names = $variable['multiple'];
  }
  else {
    // Array with unknown elements, we add an element for each existing one
    $names = $value ? array_combine(array_keys($value), array_keys($value)) : array();
  }
  // Now build elements with the right names
  foreach ($names as $key => $title) {
    if (isset($value[$key]) && is_array($value[$key])) {
      // This element is an array, we cannot edit it but we need to add it to the form
      $element[$key] = array('#type' => 'value', '#value' => $value[$key]);
      $element['variable_element_array_' . $key] = array('#type' => 'item', '#title' => $title, '#markup' => variable_format_array($value[$key]));
    }
    elseif (!empty($value[$key])) {
      $element[$key] = $item['element'] + array(
        '#title' => $title,
        '#default_value' => $value[$key],
      );
    }
  }

  $element[] = $item['element'] + array(
    '#title' => t('New'),
    '#default_value' => '',
  );

  return $element;
}

function git_hooker_php_var_executables_format($variable, $options = array()) {
  $item_list = array(
    'items' => array(),
  );
  $values = $variable['value'];
  uasort($values, 'git_hooker_php_sort_version');
  foreach ($values as $value) {
    if (!$value) {
      continue;
    }
    $item_list['items'][] = check_plain(git_hooker_php_version($value, TRUE)) . ' - ' . check_plain($value);
  }
  return theme('item_list', $item_list);
}
