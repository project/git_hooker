<?php

/**
 * @file
 * Administration functions. Forms and callbacks.
 */

/**
 * FormAPI form builder callback for settings form.
 */
/*
function git_hooker_php_settings_form($form, &$form_state) {
  $key = 'executables';
  $form[$key] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => t('PHP executables'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $executables = variable('git_hooker_php_exeutables');

  return system_settings_form($form);
}
 */

/**
 * FormAPI form validate callback for settings form.
 */
/*
function git_hooker_php_settings_form_validate(&$form, &$form_state) {
}
 */

/**
 * FormAPI form submit callback for settings form.
 */
/*
function git_hooker_php_settings_form_submit(&$form, &$form_state) {
}
 */
