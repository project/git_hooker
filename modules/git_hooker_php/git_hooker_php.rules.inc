<?php

/**
 * @file
 * Rules integration
 */

/**
 * Implements hook_rules_action_info().
 */
function git_hooker_php_rules_action_info() {
  return git_hooker_pick_rules_items('action', 'git_hooker_php');
}

/**
 * Implements hook_rules_condition_info().
 */
function git_hooker_php_rules_condition_info() {
  return git_hooker_pick_rules_items('condition', 'git_hooker_php');
}

/**
 * Implements hook_rules_file_info().
 */
function git_hooker_php_rules_file_info() {
  return git_hooker_pick_rules_files('git_hooker_php');
}
