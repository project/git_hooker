<?php

/**
 * @file
 * phplint action plugin for githook.
 * Git staging area != working copy
 *
 * @addtogroup rules
 */

function _git_hooker_php_rules_action_lint_info() {
  return array(
    'label' => t('PHP lint'),
    'help' => t('Syntax check only (lint)'),
    'parameter' => array(
      'path' => array(
        'type' => 'text',
        'label' => t('Path'),
        'default mode' => 'selector',
      ),
      'list' => array(
        'type' => 'list<text>',
        'label' => t('Files'),
        'default mode' => 'selector',
      ),
      'php_min' => array(
        'type' => 'text',
        'label' => t('Minimum PHP version'),
        'ui class' => 'RulesDataUIGitHookerTextfield',
      ),
      'php_max' => array(
        'type' => 'text',
        'label' => t('Maximum PHP version'),
        'ui class' => 'RulesDataUIGitHookerTextfield',
      ),
    ),
    'provides' => array(
      'list_true' => array(
        'type' => 'list<text>',
        'label' => t('PHP lint: files true'),
      ),
      'list_false' => array(
        'type' => 'list<text>',
        'label' => t('PHP lint: files false'),
      ),
    ),
  );
}

/**
 * Implements hook_rules_action_ACTION().
 *
 * @param array $parameters
 * @code = array(
 *   'repository' => GitHookerRepository,
 *   'php_min' => string
 *   'php_max' => string
 *   'files' => array,
 *   'filters' => list<git_hooker_regex_filter>,
 *   'settings' => array,
 *   'state' => RulesState,
 * ) @endcode
 *
 * @param RulesAction $rules_action
 *
 * @param string $op
 */
function git_hooker_php_rules_action_lint(array $parameters, RulesAction $rules_action, $op) {
  switch ($op) {
    case 'execute':
      $return = array(
        'list_true' => array(),
        'list_false' => array(),
      );

      $exe = git_hooker_php_executable_by_min_max($parameters['php_min'], $parameters['php_max']);
      if (!$exe) {
        drush_set_error(
          'git_hooker_php_executable',
          dt('Missing PHP executable. Required min: !min, max: !max',
            array('!min' => $parameters['php_min'], '!max' => $parameters['php_max'])
          )
        );
        return $return;
      }

      // root directory handling
      $path = ($parameters['path'] ? rtrim($parameters['path'], '/') . '/' : '');
      foreach ($parameters['list'] as $filename) {
        if (!is_file($path . $filename)) {
          continue;
        }

        if (drush_shell_cd_and_exec($path, 'php -l ' . escapeshellarg($filename))) {
          $return['list_true'][] = $filename;
        }
        else {
          $return['list_false'][$filename] = drush_shell_exec_output();
        }
      }

      if ($return['list_false']) {
        $indent = 8;
        drush_set_error('git_hooker_php_lint', dt('PHP syntax error'));
        foreach ($return['list_false'] as $filename => $lines) {
          drush_print($filename, 4);
          drush_print(trim(str_replace("\n", "\n" . str_repeat(' ', $indent), implode("\n", $lines))), $indent);
        }
        drush_print();
        $return['list_false'] = array_keys($return['list_false']);
      }
      else {
        drush_log(dt('PHP Syntax OK'), 'success');
      }

      return $return;

  }
}
