<?php

/**
 * @file
 * Rules condition to compare PHP version strings.
 */

function _git_hooker_php_rules_condition_version_info() {
  return array(
    'label' => t('Compare PHP version strings'),
    'parameter' => array(
      'repository' => FALSE,
      'version_a' => array(
        'label' => t('PHP version "A"'),
        'type' => 'text',
        'restriction' => 'selector',
      ),
      'operation' => array(
        'label' => t('Comparison operation'),
        'type' => 'text',
        'restriction' => 'input', // why?
        'options list' => 'git_hooker_php_version_compare_options',
      ),
      'version_b' => array(
        'label' => t('PHP version "B"'),
        'type' => 'text',
        'ui class' => 'RulesDataUIGitHookerTextfield',
        'default mode' => 'input',
      ),
    ),
  );
}

function git_hooker_php_rules_condition_version($parameters, $rules_action, $op) {
  switch ($op) {
    case 'execute':
      $result = version_compare($parameters['version_a'], $parameters['version_b']);
      return (
        ($result == -1 && in_array($parameters['operation'], array('lt', 'lte')))
        ||
        ($result == 0 && in_array($parameters['operation'], array('lte', 'eq', 'gte')))
        ||
        ($result == 1 && in_array($parameters['operation'], array('gte', 'gt')))
      );

  }
}
