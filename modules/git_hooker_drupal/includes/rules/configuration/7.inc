<?php

/**
 * @file
 * Rules configuration
 */

function _git_hooker_drupal_rules_configuration_7_info() {
  return '{ "git_hooker_drupal_7" : {
    "LABEL" : "git_hooker_drupal",
    "PLUGIN" : "reaction rule",
    "TAGS" : [ "Git" ],
    "REQUIRES" : [ "rules", "git_hooker", "git_hooker_php", "git_hooker_drupal" ],
    "ON" : [ "git_hooker_pre_commit" ],
    "IF" : [
      { "data_is" : {
          "data" : [ "git-hooker-repository:drupal-component" ],
          "op" : "\\u003E",
          "value" : "0"
        }
      }
    ],
    "DO" : [
      { "git_hooker_filetypefilter" : {
          "USING" : {
            "list" : [ "git-hooker-repository:status-staged" ],
            "filetype" : "non plain text"
          },
          "PROVIDE" : {
            "list_true" : { "list_true_nonplaintext" : "Files non plain text TRUE" },
            "list_false" : { "list_false_nonplaintext" : "Files non plain text FALSE" }
          }
        }
      },
      { "component_git_hooker_fnenceol" : {
          "path" : [ "git-hooker-repository:path" ],
          "files" : [ "list-false-nonplaintext" ],
          "encoding" : [ "git-hooker-repository:drupal-encoding" ],
          "eol" : [ "git-hooker-repository:drupal-eol" ]
        }
      },
      { "git_hooker_filetypefilter" : {
          "USING" : {
            "list" : [ "git-hooker-repository:status-staged" ],
            "filetype" : "drupal_php"
          },
          "PROVIDE" : {
            "list_true" : { "list_true_drupal_php" : "Drupal PHP files" },
            "list_false" : { "list_false_drupal_php" : "Drupal PHP files" }
          }
        }
      },
      { "git_hooker_php_lint" : {
          "USING" : {
            "repository" : [ "git-hooker-repository" ],
            "path" : [ "git-hooker-repository:path" ],
            "list" : [ "list-true-drupal-php" ],
            "php_min" : [ "git-hooker-repository:drupal-php-min" ],
            "php_max" : "7"
          },
          "PROVIDE" : {
            "list_true" : { "list_true_phplint" : "PHP lint: files true" },
            "list_false" : { "list_false_phplint" : "PHP lint: files false" }
          }
        }
      },
      { "git_hooker_drupal_coder_review" : {
          "USING" : {
            "repository" : [ "git-hooker-repository" ],
            "list" : [ "list-true-phplint" ],
            "coder_review_upgrade47" : 0,
            "coder_review_upgrade5x" : 0,
            "coder_review_upgrade6x" : 0,
            "coder_review_upgrade7x" : 1,
            "coder_review_style" : 1,
            "coder_review_comment" : 1,
            "coder_review_sql" : 1,
            "coder_review_security" : 1,
            "coder_review_i18n" : 1,
            "coder_review_i18n_po" : 0,
            "coder_severity" : "1"
          },
          "PROVIDE" : {
            "list_true" : { "list_true_drupal_coder_review" : "Drupal coder review OK." },
            "list_false" : { "list_false_drupal_coder_review" : "Drupal coder review FAIL." }
          }
        }
      }
    ]
  }
}';
}
