<?php

/**
 * @file
 * Coder module review action plugin for Git Hooker.
 * Git staging area != working copy
 *
 * @addtogroup rules
 */

function _git_hooker_drupal_rules_action_coder_review_info() {
  if (!module_exists('coder_review')) {
    return NULL;
  }

  $return = array(
    'label' => t('Drupal code review'),
    'help' => t('Code review of Coder module'),
    'parameter' => array(
      'list' => array(
        'type' => 'list<text>',
        'label' => t('Files'),
        'default mode' => 'selector',
      ),
    ),
    'provides' => array(
      'list_true' => array(
        'type' => 'list<text>',
        'label' => t('Drupal coder review OK.'),
      ),
      'list_false' => array(
        'type' => 'list<text>',
        'label' => t('Drupal coder review FAIL.'),
      ),
    ),
  );

  $reviews = _coder_review_reviews();
  $review_sort = array();
  foreach ($reviews as $name => $review) {
    $review_sort[$name] = $review['#title'];
  }
  asort($review_sort);

  $default_values = array(
    'coder_review_style' => TRUE,
    'coder_review_comment' => TRUE,
    'coder_review_sql' => TRUE,
    'coder_review_security' => TRUE,
    'coder_review_i18n' => TRUE,
  );

  foreach (array_keys($review_sort) as $name) {
    $return['parameter']["coder_review_{$name}"] = array(
      'type' => 'boolean',
      'label' => $reviews[$name]['#title'],
      'description' => isset($reviews[$name]['#description']) ? $reviews[$name]['#description'] : '',
    );

    if (isset($default_values["coder_review_{$name}"])) {
      $return['parameter']["coder_review_{$name}"]['default value'] = $default_values["coder_review_{$name}"];
    }
  }

  $return['parameter']['coder_severity'] = array(
    'type' => 'integer',
    'label' => t('Show warnings at or above the severity warning level'),
    'options list' => 'git_hooker_drupal_coder_review_severity_options',
    'default value' => SEVERITY_MINOR,
    'ui class' => 'RulesDataUIGitHookerSelect',
  );

  return $return;
}

function git_hooker_drupal_rules_action_coder_review($parameters, $rules_action, $op) {
  switch ($op) {
    case 'execute':
      $return = array(
        'list_true' => array(),
        'list_false' => array(),
      );

      if (!module_exists('coder_review')) {
        return $return;
      }

      $reviews_available = _coder_review_reviews();
      $coder_args_default = array(
        '#reviews' => array(),
        '#severity' => $parameters['coder_severity'],
        '#filename' => '',
        '#patch' => '',
        '#php_extensions' => array('inc', 'php', 'install', 'test', 'profile', 'engine'),
        '#include_extensions' => array(),
        '#ignore_lines' => array(),
        '#cache' => FALSE,
      );

      foreach ($reviews_available as $name => $review) {
        if ($parameters["coder_review_{$name}"]) {
          $coder_args_default['#reviews'][$name] = $review;
        }
      }

      $results = array();
      $results_stats = array('minor' => 0, 'normal' => 0, 'critical' => 0, 'ignored' => 0);
      foreach ($parameters['list'] as $filename) {
        $coder_args = $coder_args_default;
        $coder_args['#filename'] = "{$parameters['repository']->path}/{$filename}";
        //drush_print_r($coder_args['#filename']);
        // Read the file.
        if (_coder_review_read_and_parse_file($coder_args)) {
          // Do all of the code reviews.
          $errors = array();
          foreach ($coder_args['#reviews'] as $review_name => $review) {
            $review['#review_name'] = $review_name;
            if ($result = do_coder_review($coder_args, $review)) {
              // ignored isn't a severity level, but is a stat we need to track.
              foreach (array('critical', 'normal', 'minor', 'ignored') as $severity_level) {
                if (isset($result['#stats'][$severity_level])) {
                  $results_stats[$severity_level] += $result['#stats'][$severity_level];
                }
              }
              $errors += $result;
            }
          }

          // Theme the error messages.
          if ($errors) {
            foreach ($errors as $key => $error) {
              if (is_numeric($key)) {
                $error['warning'] = _coder_review_warning($error['rule']);
                $error['filename'] = $filename;
                $results[$filename][$key] = array('error' => $error);
              }
            }

            $return['list_false'][] = $filename;
            if ($results[$filename]) {
              ksort($results[$filename], SORT_NUMERIC);
            }
          }

          // Sort the results.
        }
        else {
          $results[$filename][] = array(
            'warning' => t('Could not read the file'),
            'severity_name' => 'critical',
          );
        }
      }

      $results_stats_ignored = $results_stats['ignored'];
      unset($results_stats['ignored']);
      if (array_sum($results_stats)) {
        drush_set_error('git_hooker_drupal_coder_review', dt('Drupal cored review FAIL'));
        //drush_print_r($results);
        $rows = array();
        $row = 0;
        foreach ($results as $filename => $result) {
          $rows[$row++] = array('data' => array(
            array(
              'colspan' => 3,
              'data' => $filename,
            ),
          ));
          foreach ($result as $review_info) {
            if (!empty($review_info['error']['warning']['#warning'])) {
              //drush_print_r($review_info);
              $warning = '';
              if (is_string($review_info['error']['warning'])) {
                $warning = $review_info['error']['warning'];
              }
              elseif (is_string($review_info['error']['warning']['#warning'])) {
                $warning = $review_info['error']['warning']['#warning'];
              }
              $rows[$row++] = array('data' => array(
                array('data' => drupal_strtoupper($review_info['error']['severity_name'])),
                array('data' => $review_info['error']['lineno'], 'align' => GitHookerTextTable::ALIGN_RIGHT),
                array('data' => $warning),
              ));

              if (!empty($review_info['error']['line'])) {
                $rows[$row++] = array('data' => array(
                  array(
                    'colspan' => 3,
                    'data' => trim($review_info['error']['line']),
                  ),
                ));
              }
            }
            else{
              drush_print_r($review_info);
            }
          }
        }

        if ($rows) {
          $table = new GitHookerTextTable();
          $table->setRows($rows);
          drush_print($table->getOutput());
          drush_print();
        }
      }
      else {
        drush_log(dt('Drupal coder review: OK'), 'success');
      }

      $return['list_true'] = array_diff($parameters['list'], $return['list_false']);
      return $return;
  }
}
