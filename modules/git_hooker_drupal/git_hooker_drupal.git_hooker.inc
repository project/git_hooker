<?php

/**
 * @file
 * Git Hooker integration.
 */

/**
 * Implements hook_git_hooker_filetype_info().
 */
function git_hooker_drupal_git_hooker_filetype_info() {
  return array(
    'drupal_php' => array(
      'label' => 'PHP',
      'group' => t('Drupal'),
      'mimetype' => array('text/x-php4-src', 'text/x-php3-src', 'text/vnd.wap.wml', 'application/x-php'),
      'filters' => array(
        array('weight' => 0, 'invert' => FALSE, 'exclude' => FALSE, 'pattern' => '@(/|^).*\.php$@'),
        array('weight' => 1, 'invert' => FALSE, 'exclude' => FALSE, 'pattern' => '@(/|^).*\.inc$@'),
        array('weight' => 2, 'invert' => FALSE, 'exclude' => FALSE, 'pattern' => '@(/|^).*\.module$@'),
        array('weight' => 3, 'invert' => FALSE, 'exclude' => FALSE, 'pattern' => '@(/|^).*\.install$@'),
        array('weight' => 5, 'invert' => FALSE, 'exclude' => FALSE, 'pattern' => '@(/|^).*\.engine$@'),
        array('weight' => 6, 'invert' => FALSE, 'exclude' => FALSE, 'pattern' => '@(/|^).*\.profile$@'),
        array('weight' => 7, 'invert' => FALSE, 'exclude' => TRUE,  'pattern' => '@^core/vendor/*.@'),
      ),
    ),
  );
}
