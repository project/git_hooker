<?php

/**
 * @file
 * Default rules.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function git_hooker_drupal_default_rules_configuration() {
  return git_hooker_pick_rules_items('configuration', 'git_hooker_drupal');
}
