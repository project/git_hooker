<?php

/**
 * @file
 * Rules module integration
 */

/**
 * Implements hook_rules_action_info().
 */
function git_hooker_drupal_rules_action_info() {
  return git_hooker_pick_rules_items('action', 'git_hooker_drupal');
}

/**
 * Implements hook_rules_file_info().
 */
function git_hooker_drupal_rules_file_info() {
  return git_hooker_pick_rules_files('git_hooker_drupal');
}

/**
 * Implements hook_rules_data_info_alter(),
 *
 * drupal
 * drupal_module
 * drupal_feature
 * drupal_theme
 * drupal_profile
 * drupal_theme_engine
 * drupal_site
 * drupal_distribution
 * drupal_core
 * drupal_version
 */
function git_hooker_drupal_rules_data_info_alter(&$info) {
  if (isset($info['git_hooker_repository'])) {
    $info['git_hooker_repository']['property info']['drupal_component'] = array(
      'type' => 'integer',
      'label' => t('Number of Drupal components'),
      'description' => t('Modules, themes, installation profiles'),
      'getter callback' => 'git_hooker_drupal_property_data_verbatim',
    );

    $info['git_hooker_repository']['property info']['drupal_module'] = array(
      'type' => 'integer',
      'label' => t('Number of Drupal modules'),
      'getter callback' => 'git_hooker_drupal_property_data_verbatim',
    );

    $info['git_hooker_repository']['property info']['drupal_theme'] = array(
      'type' => 'integer',
      'label' => t('Number of Drupal profiles'),
      'getter callback' => 'git_hooker_drupal_property_data_verbatim',
    );

    $info['git_hooker_repository']['property info']['drupal_profile'] = array(
      'type' => 'integer',
      'label' => t('Number of Drupal themes'),
      'getter callback' => 'git_hooker_drupal_property_data_verbatim',
    );

    $info['git_hooker_repository']['property info']['drupal_php_min'] = array(
      'type' => 'text',
      'label' => t('Number of Drupal themes'),
      'getter callback' => 'git_hooker_drupal_property_data_verbatim',
    );

    $info['git_hooker_repository']['property info']['drupal_eol'] = array(
      'type' => 'text',
      'label' => t('End of line marker'),
      'getter callback' => 'git_hooker_drupal_property_data_verbatim',
      'options list' => 'git_hooker_endofline_options',
    );

    $info['git_hooker_repository']['property info']['drupal_encoding'] = array(
      'type' => 'text',
      'label' => t('Encoding'),
      'getter callback' => 'git_hooker_drupal_property_data_verbatim',
      'options list' => 'git_hooker_encoding_options',
    );
  }
}

/**
 * @todo improve
 * @todo Drupal version
 *
 * @param GitHookerRepository $repo
 */
function git_hooker_drupal_repo_informations($repo) {
  $return = array(
    'drupal_component' => 0,
    'drupal_module' => 0,
    'drupal_feature' => 0,
    'drupal_theme' => 0,
    'drupal_theme_engine' => 0,
    'drupal_profile' => 0,
    'drupal_distribution' => 0,
    'drupal_core' => 0,
    'drupal_site' => 0,
    'drupal_version' => 0,
    'drupal_php_min' => 0,
    'drupal_eol' => 'unix',
    'drupal_encoding' => 'utf-8',
  );

  // @todo file_scan_directory().
  if (drush_shell_exec(sprintf('find %s -type f -name "*.info"', escapeshellarg($repo->path)))) {
    $info_files = drush_shell_exec_output();

    // Exclude *.info files from sub repositories.
    foreach (array_keys($info_files) as $i) {
      foreach (array_keys($repo->submodules) as $submodule_path) {
        $submodule_path_full = "{$repo->path}/{$submodule_path}/";
        if (strpos($info_files[$i], $submodule_path_full) === 0) {
          unset($info_files[$i]);
        }
      }
    }

    $drupal_cores = array();
    foreach ($info_files as $info_file) {
      $info_file_pathinfo = pathinfo($info_file);
      $info_file_dir = $info_file_pathinfo['dirname'];
      $info_file_name = $info_file_pathinfo['filename'];

      $type = NULL;
      if (file_exists("{$info_file_dir}/{$info_file_name}.module")) {
        $type = 'module';
      }
      elseif (file_exists("{$info_file_dir}/{$info_file_name}.profile")) {
        $type = 'profile';
      }
      elseif (file_exists("{$info_file_dir}/template.php")) {
        $type = 'theme';
      }

      if ($type) {
        // @todo Error handling
        $info = drupal_parse_info_file($info_file);
        if (!empty($info['core'])) {
          $matches = NULL;
          if (preg_match('/^([1-9]\d*)/', $info['core'], $matches)) {
            if (empty($drupal_cores[$matches[1]])) {
              $drupal_cores[$matches[1]] = 1;
            }
            else {
              $drupal_cores[$matches[1]]++;
            }
          }

          $return["drupal_{$type}"]++;
          $return["drupal_component"]++;
        }
      }
    }
  }

  if (
    file_exists("{$repo->path}/settings.php")
    &&
    (
      is_dir("{$repo->path}/modules")
      ||
      is_dir("{$repo->path}/themes")
      ||
      is_dir("{$repo->path}/libraries")
    )
  ) {
    $return['drupal_site'] = 1;
    $return['drupal_component']++;
  }

  if (count($drupal_cores) == 1) {
    $return['drupal_version'] = key($drupal_cores);
    switch ($return['drupal_version']) {
      case 6:
        $return['drupal_php_min'] = '4.4.0';
        break;

      case 7:
        $return['drupal_php_min'] = '5.2.5';
        break;

      case 8:
        $return['drupal_php_min'] = '5.3';
        break;

    }
  }

  return $return;
}
