<?php

/**
 * @file
 * GitHookerRepository class
 */

/**
 * Repo object
 */
class GitHookerRepository {
  // <editor-fold defaultstate="collapsed" desc="Fields">
  /**
   * HEADless sha
   *
   * @var string EMPTY_SHA
   */
  const EMPTY_SHA = '4b825dc642cb6eb9a060e54bf8d69288fbee4904';

  /**
   * Filesystem path to repository.
   *
   * @var string $path
   */
  public $path = NULL;

  /**
   * @var Git2\Repository $repository
   */
  public $repository = NULL;

  /**
   * Current branch
   *
   * Posible values:
   *  - [empty string]
   *    Git repo is initialized, but has no any commmit.
   *  - (no branch)
   *    Not the HEAD is checked out.
   *  - [any string]
   *    The branch name
   *
   * @var string $currentBranch
   */
  public $currentBranch = NULL;

  /**
   * Full key => value
   *
   * @var array $configFlat
   */
  public $configFlat = array();

  /**
   * Nested array
   *
   * @var array $configTree
   */
  public $configTree = array();

  protected $configDefaults = array(
    'remote' => array()
  );

  /**
   * Status
   *
   * @var array $status
   */
  public $status = array(
    '' => array(),
    'M' => array(),
    'A' => array(),
    'D' => array(),
    'R' => array(),
    'C' => array(),
    'U' => array(),
    '??' => array(),
    '!!' => array(),
    'staged' => array(),
    'dirty' => array(),
  );

  public $remotes = array();

  public $submodules = array();

  /**
   * Data container for modules.
   * @var array $data
   */
  public $data = array();
  // </editor-fold>

  function __construct($path) {
    $this->path = $path;
    $this->initialize();
  }

  protected function initialize() {
    //$this->repository = new Git2\Repository($this->path);
    $this->initializeConfig();
    $this->initializeCurrentBranch();
    $this->initializeStatus();
    $this->initializeRemotes();
    $this->initializeSubmodules();
  }

  /**
   * Command line based Git config parser.
   */
  protected function initializeConfig() {
    if (drush_shell_cd_and_exec($this->path, 'git config -l')) {
      $lines = drush_shell_exec_output();
      foreach ($lines as $line) {
        list($key_full, $value) = explode('=', $line, 2);
        $value = $this->configValue($value, $key_full);
        $this->configFlat[$key_full] = $value;
        $keys = explode('.', $key_full);
        $this->configSet($this->configTree, $keys, $value);
      }
      $this->configTree = drupal_array_merge_deep($this->configDefaults, $this->configTree);
    }
  }

  /**
   * Recursion helper function.
   *
   * @see initializeConfig()
   */
  protected function configSet(&$config, $keys, $value) {
    $key = array_shift($keys);
    if ($keys) {
      if (!isset($config[$key])) {
        $config[$key] = array();
      }
      $this->configSet($config[$key], $keys, $value);
    }
    else {
      $config[$key]['#value'] = $value;
    }
  }

  public function configValue($value, $key = NULL) {
    if ($value === 'false') {
      $value = FALSE;
    }
    elseif ($value === 'true') {
      $value = TRUE;
    }

    return $value;
  }

  /**
   * Command line result parser.
   *
   * Get the first branch name prefixed with asterix.
   */
  protected function initializeCurrentBranch() {
    if (!drush_shell_cd_and_exec($this->path, 'git branch')) {
      return;
    }

    $matches = NULL;
    $output = implode("\n", drush_shell_exec_output());
    if (preg_match('/^\s*\*\s*(?P<branch>.+)\s*$/mu', $output, $matches)) {
      $this->currentBranch = $matches['branch'];
    }
  }

  /**
   * array(
   *   M => array()
   *   A => array()
   *   D => array()
   *   R => array(from => to)
   *   C => array()
   *   U => array()
   *   ?? => array()
   *   !! => array()
   *   staged => array()
   *   dirty => array()
   * )
   */
  protected function initializeStatus() {
    if (!drush_shell_cd_and_exec($this->path, 'git status --porcelain -z')) {
      return;
    }

    $lines = drush_shell_exec_output();
    if (!$lines) {
      return;
    }

    $staged_statuses = array('A', 'D', 'R', 'C');

    $lines = explode("\0", $lines[0]);
    $i = 0;
    $lines_count = count($lines);
    while ($i < $lines_count) {
      list($statuses, $filename) = preg_split('/\s+/', $lines[$i], 2);
      $statuses = ($statuses == '!!' || $statuses == '??') ?
      array($statuses)
      :
      str_split($statuses, 1)
      ;

      foreach ($statuses as $status) {
        if ($status == 'R') {
          $this->status[$status][$lines[++$i]] = $filename;
        }
        else {
          $this->status[$status][] = $filename;
        }

        if (in_array($status, $staged_statuses)) {
          $this->status['staged'][] = $filename;
        }
      }
      $i++;
    }

    $this->status['dirty'] = array_intersect($this->status['staged'], $this->status['M']);
  }

  protected function initializeRemotes() {
    $matches = NULL;
    foreach ($this->configFlat as $key => $value) {
      if (preg_match('/^remote\.([^\.]+)\.url$/', $key, $matches)) {
        $this->remotes[$matches[1]] = $value;
      }
    }
  }

  protected function initializeSubmodules() {
    if (!is_file("{$this->path}/.gitmodules")) {
      return;
    }

    // @todo Error handling
    $ini = parse_ini_file("{$this->path}/.gitmodules", TRUE);
    foreach ($ini as $group => $info) {
      if (strpos($group, 'submodule ') === 0 && !empty($info['path']) && !empty($info['url'])) {
        $this->submodules[$info['path']] = $info['url'];
      }
    }
  }
}
