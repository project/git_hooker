<?php

/**
 * @file
 * Rules data wrapper
 */

class GitHookerRegexFilterDataWrapper extends RulesIdentifiableDataWrapper {

  protected function extractIdentifier($data) {
    return $data->__toString();
  }

  protected function load($id) {
    return git_hooks_regex_filter_load($id);
  }
}
