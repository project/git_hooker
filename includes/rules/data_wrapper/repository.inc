<?php

/**
 * @file
 * Rules data wrapper
 */

class GitHookerRepositoryDataWrapper extends RulesIdentifiableDataWrapper {

  protected function extractIdentifier($data) {
    return $data->path;
  }

  protected function load($id) {
    return git_hooks_repository_load($id);
  }
}
