<?php

/**
 * @file
 * Rules action
 */

function _git_hooker_rules_action_filetypefilter_info() {
  return array(
    'label' => t('Filter file names in a list by file type'),
    'parameter' => array(
      'repository' => FALSE,
      'list' => array(
        'type' => 'list<text>',
        'label' => t('Files', array(), array('context' => 'data_types')),
        'description' => t('The data list for which an item is to be removed.'),
        'restriction' => 'selector',
        'default mode' => 'selector', // @todo maybe this is not necessary
      ),
      'filetype' => array(
        'type' => 'text',
        'label' => t('File type'),
        'default mode' => 'input',
        'options list' => 'git_hooker_filetype_options',
      ),
    ),
    'provides' => array(
      'list_true' => array(
        'type' => 'list<text>',
        'label' => t('File type filter: files true.'),
      ),
      'list_false' => array(
        'type' => 'list<text>',
        'label' => t('File type filter: files false.'),
      ),
    ),
  );
}

function git_hooker_rules_action_filetypefilter($parameters, $rules_action, $op) {
  switch ($op) {
    case 'execute':
      $return = array(
        'list_true' => array(),
        'list_false' => array(),
      );
      $filetype = git_hooker_info_filetype($parameters['filetype']);
      GitHookerRegexFilter::filters($filetype['filters'], $parameters['list'], $return['list_true']);
      $return['list_false'] = array_diff($parameters['list'], $return['list_true']);
      return $return;
  }
}
