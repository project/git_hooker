<?php

/**
 * @file
 * File charachter set detection action plugin for githook.
 *
 * @addtogroup rules
 */

function _git_hooker_rules_action_filename_info() {
  return array(
    'label' => t('Check file names'),
    'help' => t('Check file names'),
    'parameter' => array(
      'repository' => FALSE,
      'list' => array(
        'type' => 'list<text>',
        'label' => t('Files'),
        'default mode' => 'selector',
      ),
      'filters' => array(
        'type' => 'list<git_hooker_regex_filter>',
        'label' => t('Filters'),
        'default mode' => 'input',
        'default value' => array(
          array(
            'pattern' => '@^[\\\\/a-z0-9_\.\-]+$@i',
            'invert' => FALSE,
            'exclude' => FALSE,
            'weight' => 0,
          ),
        ),
      ),
    ),
    'provides' => array(
      'list_true' => array(
        'type' => 'list<text>',
        'label' => t('File name: files true'),
      ),
      'list_false' => array(
        'type' => 'list<text>',
        'label' => t('File name: files false'),
      ),
    ),
  );
}

/**
 * Implements hook_rules_action_ACTION().
 */
function git_hooker_rules_action_filename($parameters, $rules_action, $op) {
  switch ($op) {
    case 'execute':
      $return = array(
        'list_true' => array(),
        'list_false' => array(),
      );

      // @todo File is deleted, and it has an invalid name.
      GitHookerRegexFilter::filters($parameters['filters'], $parameters['list'], $return['list_true']);
      $return['list_false'] = array_diff($parameters['list'], $return['list_true']);

      if ($return['list_false']) {
        // @todo This error message is depend from the $parameters['patterns'].
        drush_set_error('git_hooker_filename', dt('Attempt to add a non-ascii file name.'));
        drush_print(dt('This can cause problems if you want to work with people on other platforms.'), 4);
        drush_print(dt('To be portable it is advisable to rename the falowing files:'), 4);
        foreach ($return['list_false'] as $file) {
          drush_print($file, 8);
        }
        drush_print();
      }
      else {
        drush_log(dt('File names: OK'), 'success');
      }

      return $return;

  }
}
