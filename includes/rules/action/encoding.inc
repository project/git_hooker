<?php

/**
 * @file
 * File charachter set detection action plugin for githook.
 *
 * @addtogroup rules
 */

function _git_hooker_rules_action_encoding_info() {
  return array(
    'label' => t('Detect character encoding'),
    'help' => t('Detect character encoding'),
    'parameter' => array(
      'repository' => FALSE,
      'path' => array(
        'type' => 'text',
        'label' => t('Path'),
        'default mode' => 'selector',
      ),
      'list' => array(
        'type' => 'list<text>',
        'label' => t('Files'),
        'default mode' => 'selector',
      ),
      'encoding' => array(
        'type' => 'text',
        'label' => t('Encoding'),
        'options list' => 'git_hooker_encoding_options',
        'default value' => 'utf-8',
        'default mode' => 'input',
      ),
    ),
    'provides' => array(
      'list_true' => array(
        'type' => 'list<text>',
        'label' => t('Charset: files true'),
      ),
      'list_false' => array(
        'type' => 'list<text>',
        'label' => t('Charset: files false'),
      ),
    ),
  );
}

/**
 * Implements hook_rules_action_ACTION().
 */
function git_hooker_rules_action_encoding($parameters, $rules_action, $op) {
  switch ($op) {
    case 'execute':
      $return = array(
        'list_true' => array(),
        'list_false' => array(),
      );

      $path = ($parameters['path'] ? rtrim($parameters['path'], '/') . '/' : '');
      $encoding = ($parameters['encoding'] == 'utf-8-bom' ? 'utf-8' : $parameters['encoding']);
      foreach ($parameters['list'] as $filename) {
        if (!is_file($path . $filename)) {
          continue;
        }

        // @todo is_readable.
        // @todo BOM check
        if (mb_detect_encoding(file_get_contents($path . $filename), $encoding)) {
          $return['list_true'][] = $filename;
        }
        else {
          $return['list_false'][] = $filename;
        }
      }

      if ($return['list_false']) {
        drush_set_error(dt('Encoding of files: ERROR'));
        foreach ($return['list_false'] as $filename) {
          drush_print($filename, 4);
        }
        drush_print();
      }
      else {
        drush_log(dt('Encoding of files: OK'), 'success');
      }

      return $return;
  }
}
