<?php

/**
 * @file
 * Rules action
 */

function _git_hooker_rules_action_listtextfilter_info() {
  return array(
    'group' => t('Data'),
    'label' => t('Filter items from a list by regular expressions'),
    'parameter' => array(
      'repository' => FALSE,
      'path' => array(
        'type' => 'text',
        'label' => t('Path'),
        'default mode' => 'selector',
      ),
      'list' => array(
        'type' => 'list<text>',
        'label' => t('List', array(), array('context' => 'data_types')),
        'description' => t('The data list for which an item is to be filtered.'),
        'restriction' => 'selector',
        'default mode' => 'selector',
      ),
      'filters' => array(
        'type' => 'list<git_hooker_regex_filter>',
        'label' => t('Filters'),
        'optional' => TRUE,
        'default mode' => 'input',
      ),
    ),
    'provides' => array(
      'list_true' => array(
        'type' => 'list<text>',
        'label' => t('Text list filter: Items selected by regular expressions.'),
      ),
      'list_false' => array(
        'type' => 'list<text>',
        'label' => t('Text list filter: Items not selected by regular expressions.'),
      ),
    ),
  );
}

function git_hooker_rules_action_listtextfilter($parameters, $rules_action, $op) {
  switch ($op) {
    case 'execute':
      $return = array(
        'list_true' => array(),
        'list_false' => array(),
      );
      GitHookerRegexFilter::filters($parameters['filters'], $parameters['list'], $return['list_true']);
      $return['list_false'] = array_diff($parameters['list'], $return['list_true']);
      return $return;
  }
}
