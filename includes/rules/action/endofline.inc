<?php

/**
 * @file
 * New line detection action plugin for githook.
 *
 * @addtogroup rules
 */

function _git_hooker_rules_action_endofline_info() {
  return array(
    'label' => t('Detect end of line line charachter.'),
    'help' => t("Dos, Mac, Unix new line detection. This is a nativ feature in Git. Configuration options: core.eol, core.safecrlf, core.autocrlf"),
    'parameter' => array(
      'repository' => FALSE,
      'path' => array(
        'type' => 'text',
        'label' => t('Path'),
        'default mode' => 'selector',
      ),
      'list' => array(
        'type' => 'list<text>',
        'label' => t('Files'),
        'default mode' => 'selector',
      ),
      'endofline' => array(
        'type' => 'text',
        'label' => t('End of line'),
        'options list' => 'git_hooker_endofline_options',
        'default mode' => 'input',
      ),
    ),
    'provides' => array(
      'list_true' => array(
        'type' => 'list<text>',
        'label' => t('End of line: files true'),
      ),
      'list_false' => array(
        'type' => 'list<text>',
        'label' => t('End of line: files false'),
      ),
    ),
  );
}

/**
 * Implements hook_rules_action_ACTION().
 */
function git_hooker_rules_action_endofline($parameters, $rules_action, $op) {
  switch ($op) {
    case 'execute':
      $return = array(
        'list_true' => array(),
        'list_false' => array(),
      );

      $path = ($parameters['path'] ? rtrim($parameters['path'], '/') . '/' : '');
      foreach ($parameters['list'] as $filename) {
        if (!is_file($path . $filename)) {
          continue;
        }

        $content = file_get_contents($path . $filename);
        $endofline = array(
          'unix' => preg_match('/(?<!\r)\n+(?!\r)/', $content),
          'dos' => preg_match('/(?<!\r)((\r\n)+)+(?!\n)/', $content),
          'mac' => preg_match('/(?<!\n)\r+(?!\n)/', $content),
        );

        $endofline_sum = array_sum($endofline);
        if ($endofline_sum == 0 || ($endofline_sum == 1 && $endofline[$parameters['endofline']])) {
          $return['list_true'][] = $filename;
        }
        else {
          $return['list_false'][$filename] = $endofline;
        }
      }

      if ($return['list_false']) {
        // @todo Call the $rules_action['options list'] callback.
        $eol_options = git_hooker_endofline_options();
        drush_set_error('git_hooker_endofline', dt('End of line markers ERROR'));
        drush_print(dt('Required end of line: !eol', array('!eol' => $eol_options[$parameters['endofline']])), 4);
        foreach ($return['list_false'] as $filename => $endofline) {
          foreach ($eol_options as $eol => $label) {
            if ($eol != $parameters['endofline'] && $endofline[$eol]) {
              drush_print($filename . ' ' . $label, 8);
            }
          }
        }
        drush_print();
        $return['list_false'] = array_keys($return['list_false']);
      }
      else {
        drush_log(dt('End of line markers OK'), 'success');
      }

      return $return;
  }
}
