<?php

/**
 * @file
 * Description of GitHookerRepository
 */

function _git_hooker_rules_data_repository_info() {
  return array(
    'label' => t('Git repository'),
    'wrapper class' => 'GitHookerRepositoryDataWrapper',
    'creation callback' => 'git_hooker_repository_load',
    'token type' => 'git_hooker_repository',
    'property info' => array(
      'path' => array(
        'type' => 'text',
        'label' => t('Path'),
        'sanitized' => TRUE,
        'getter callback' => 'entity_property_verbatim_get',
      ),
      'current_branch' => array(
        'type' => 'text',
        'label' => t('Current branch'),
        'sanitized' => TRUE,
        'getter callback' => 'entity_property_verbatim_get',
      ),
      'status_staged' => array(
        'type' => 'list<text>',
        'label' => t('Staged files'),
        'getter callback' => 'git_hooker_property_verbatim_nested_get',
        'git_hooker' => array(
          'keys' => array(
            'status',
            'staged',
          ),
        ),
      ),

      // @todo branches

      // @todo remotes
      // @todo remote_aliases
      // @todo remote_urls

      // @todo submodules
      // @todo submodule_paths
      // @todo submodule_urls
    ),
  );
}
