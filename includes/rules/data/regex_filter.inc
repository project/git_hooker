<?php

/**
 * @file
 * Rules data info
 */

/**
 * @see git_hooker_rules_data_info()
 */
function _git_hooker_rules_data_regex_filter_info() {
  return array(
    'label' => t('Regex filter'),
    'wrapper class' => 'GitHookerRegexFilterDataWrapper',
    'ui class' => 'RulesDataUIGitHookerRegexFilter',
    'property info' => array(
      'weight' => array(
        'type' => 'float',
        'label' => t('Weight'),
        'getter callback' => 'entity_property_verbatim_get',
      ),
      'exclude' => array(
        'type' => 'boolean',
        'label' => t('Exclude'),
        'getter callback' => 'entity_property_verbatim_get',
      ),
      'invert' => array(
        'type' => 'boolean',
        'label' => t('Invert'),
        'getter callback' => 'entity_property_verbatim_get',
      ),
      'pattern' => array(
        'type' => 'text',
        'label' => t('Pattern'),
        'getter callback' => 'entity_property_verbatim_get',
      ),
    ),
  );
}

function _git_hooker_rules_data_regex_filter_list_info() {
  return array(
    'ui class' => 'RulesDataUIListGitHookerRegexFilter',
  );
}

/**
 * Regex filter handler.
 *
 * Independent from Rules module.
 */
class GitHookerRegexFilter implements ArrayAccess {

  // <editor-fold defaultstate="collapsed" desc="Static methods">
  public static function defaultValue() {
    return array(
      'weight' => 0,
      'exclude' => FALSE,
      'invert' => FALSE,
      'pattern' => '',
    );
  }

  public static function isEmpty($filter) {
    return (!isset($filter['pattern']) || drupal_strlen(trim($filter['pattern'])) == 0);
  }

  public static function isPattern($pattern) {
    return (@preg_match($pattern, '') !== FALSE);
  }

  public static function filters(array $filters, array $source, array &$selected) {
    usort($filters, 'git_hooker_sort_weight');
    foreach ($filters as $filter) {
      self::filter($filter, $source, $selected);
    }
  }

  public static function filter($filter, array $source, array &$selected) {
    if (self::isEmpty($filter)) {
      return;
    }

    if ($filter['exclude']) {
      foreach (array_keys($selected) as $key) {
        $match = preg_match($filter['pattern'], $selected[$key]);
        if ((!$filter['invert'] && $match) || ($filter['invert'] && !$match)) {
          unset($selected[$key]);
        }
      }
    }
    else {
      foreach ($source as $item) {
        $match = preg_match($filter['pattern'], $item);
        if ((!$filter['invert'] && $match) || ($filter['invert'] && !$match)) {
          $selected[] = $item;
        }
      }
    }
  }
  // </editor-fold>

  // <editor-fold defaultstate="collapsed" desc="Fields">
  /**
   * @var float $weight
   */
  public $weight = 0;

  /**
   * @var boolean $exclude
   */
  public $exclude = FALSE;

  /**
   * @var boolean $invert
   */
  public $invert = FALSE;

  /**
   * @var string $pattern
   */
  public $pattern = '';
  // </editor-fold>

  /**
   * @param string $pattern
   * @param boolean $exclude
   * @param boolean $invert
   * @param integer $weight
   */
  public function __construct($pattern, $exclude = FALSE, $invert = FALSE, $weight = 0) {
    if (is_array($pattern)) {
      if (isset($pattern['exclude'])) {
        $exclude = $pattern['exclude'];
      }

      if (isset($pattern['invert'])) {
        $invert = $pattern['invert'];
      }

      if (isset($pattern['weight'])) {
        $weight = $pattern['weight'];
      }

      if (isset($pattern['pattern'])) {
        $pattern = $pattern['pattern'];
      }
      else {
        $pattern = '';
      }
    }
    $this->weight = $weight;
    $this->exclude = $exclude;
    $this->invert = $invert;
    $this->pattern = $pattern;
  }

  // <editor-fold defaultstate="collapsed" desc="Magic methods">
  public function __toString() {
    return sprintf(
      '%F|%d|%d|%s',
      $data->weight,
      $data->exclude,
      $data->invert,
      $data->pattern
    );
  }
  // </editor-fold>

  // <editor-fold defaultstate="collapsed" desc="Interface ArrayAccess">
  /**
   * ArrayAccess
   */
  public function offsetExists($offset) {
    return isset($this->$offset);
  }

  /**
   * ArrayAccess
   */
  public function offsetGet($offset) {
    return $this->$offset;
  }

  /**
   * ArrayAccess
   */
  public function offsetSet($offset, $value) {
    if ($this->offsetExists($offset)) {
      $this->$offset = $value;
    }
  }

  /**
   * ArrayAccess
   */
  public function offsetUnset($offset) {
    // do nothing
  }
  // </editor-fold>

  public function toArray() {
    return array(
      'weight' => $this->weight,
      'exclude' => $this->exclude,
      'invert' => $this->invert,
      'pattern' => $this->pattern,
    );
  }
}

// <editor-fold defaultstate="collapsed" desc="">
/**
 * UI handler for git_hooker_regex_filter data type
 *
 * @addtogroup rules
 */
class RulesDataUIGitHookerRegexFilter extends RulesDataUI implements RulesDataDirectInputFormInterface {

  public static function inputForm($name, $info, $settings, RulesPlugin $element) {
    $form = array();

    $settings += array($name => isset($info['default value']) ? $info['default value'] : GitHookerRegexFilter::defaultValue());

    $form[$name] = array(
      '#type' => 'git_hooker_regex_filter',
      '#default_value' => $settings[$name],
      '#required' => empty($info['optional']),
    );

    RulesDataInputEvaluator::attachForm($form, $settings, $info, $element->availableVariables());
    return $form;
  }

  public static function render($value) {
    $return = array(
      'content' => array('#markup' => '<pre>' . check_plain(print_r($value, TRUE)) . '</pre>'),
      '#attributes' => array('class' => array('rules-parameter-git-hooker-regex-filter')),
    );
    return $return;
  }
}

class RulesDataUIListGitHookerRegexFilter extends RulesDataUIGitHookerRegexFilter {
  public static function inputForm($name, $info, $settings, RulesPlugin $element) {
    $form = array();

    $settings += array($name => isset($info['default value']) ? $info['default value'] : array(GitHookerRegexFilter::defaultValue()));

    $form[$name] = array(
      '#type' => 'git_hooker_regex_filters',
      '#default_value' => $settings[$name],
      '#required' => empty($info['optional']),
    );

    RulesDataInputEvaluator::attachForm($form, $settings, $info, $element->availableVariables());
    return $form;
  }

  public static function render($value) {
    $return = array(
      'content' => array('#markup' => '<pre>' . check_plain(print_r($value, TRUE)) . '</pre>'),
      '#attributes' => array('class' => array('rules-parameter-list-git-hooker-regex-filter')),
    );
    return $return;
  }
}

// </editor-fold>
