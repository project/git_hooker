<?php

/**
 * @file
 * Rules configuration
 */

function _git_hooker_rules_configuration_fnenceol_info() {
  return '{ "git_hooker_fnenceol" : {
    "LABEL" : "git_hooker_fnenceol",
    "PLUGIN" : "action set",
    "TAGS" : [ "Git" ],
    "REQUIRES" : [ "git_hooker" ],
    "USES VARIABLES" : {
      "path" : { "label" : "Path", "type" : "text" },
      "files" : { "label" : "Files", "type" : "list\\u003Ctext\\u003E" },
      "encoding" : { "label" : "Encoding", "type" : "text" },
      "eol" : { "label" : "End of line", "type" : "text" }
    },
    "ACTION SET" : [
      { "git_hooker_filename" : {
          "USING" : {
            "list" : [ "files" ],
            "filters" : { "value" : [
                {
                  "pattern" : "@^[\\\\\\\\\\/a-z0-9_\\\\.\\\\-]+$@i",
                  "weight" : "0",
                  "exclude" : 0,
                  "invert" : 0
                }
              ]
            }
          },
          "PROVIDE" : {
            "list_true" : { "list_true_filename" : "File name: files true" },
            "list_false" : { "list_false_filename" : "File name: files false" }
          }
        }
      },
      { "git_hooker_encoding" : {
          "USING" : { "path" : [ "path" ], "list" : [ "files" ], "encoding" : [ "encoding" ] },
          "PROVIDE" : {
            "list_true" : { "list_true_encoding" : "Encoding: files true" },
            "list_false" : { "list_false_encoding" : "Encoding: files false" }
          }
        }
      },
      { "git_hooker_endofline" : {
          "USING" : { "path" : [ "path" ], "list" : [ "files" ], "endofline" : [ "eol" ] },
          "PROVIDE" : {
            "list_true" : { "list_true_eol" : "End of line: files true" },
            "list_false" : { "list_false_eol" : "End of line: files false" }
          }
        }
      }
    ]
  }
}';
}
