<?php

/**
 * @file
 * Rules event
 */

function _git_hooker_rules_event_post_rewrite_info() {
  return array(
    'label' => t('Git hook - Post rewrite'),
    'help' => t('This hook is invoked by commands that rewrite commits (git commit --amend, git-rebase; currently git-filter-branch does not call it!). Its first argument denotes the command it was invoked by: currently one of amend or rebase. Further command-dependent arguments may be passed in the future.<br />
The hook receives a list of the rewritten commits on stdin, in the format<br />
<old-sha1> SP <new-sha1> [ SP <extra-info> ] LF<br />
The extra-info is again command-dependent. If it is empty, the preceding SP is also omitted. Currently, no commands pass any extra-info.<br />
The hook always runs after the automatic note copying (see "notes.rewrite.<command>" in linkgit:git-config.txt) has happened, and thus has access to these notes.<br />
The following command-specific comments apply:<br />
rebase<br />
For the squash and fixup operation, all commits that were squashed are listed as being rewritten to the squashed commit. This means that there will be several lines sharing the same new-sha1.<br />
The commits are guaranteed to be listed in the order that they were processed by rebase.', _git_hooker_hook_description_args()
    ),
  );
}
