<?php

/**
 * @file
 * Rules event
 */

function _git_hooker_rules_event_commit_msg_info() {
  return array(
    'label' => t('Git hook - Commit message'),
    'help' => t('This hook is invoked by !gitcommit, and can be bypassed with !noverify option. It takes a single parameter, the name of the file that holds the proposed commit log message. Exiting with non-zero status causes the !gitcommit to abort.<br />The hook is allowed to edit the message file in place, and can be used to normalize the message into some project standard format (if the project has one). It can also be used to refuse the commit after inspecting the message file.<br />The default !commitmsg hook, when enabled, detects duplicate "Signed-off-by" lines, and aborts the commit if one is found.', _git_hooker_hook_description_args()),
  );
}
