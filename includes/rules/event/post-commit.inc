<?php

/**
 * @file
 * Rules event
 */

function _git_hooker_rules_event_post_commit_info() {
  return array(
    'label' => t('Git hook - Post commit'),
    'help' => t('This hook is invoked by !gitcommit. It takes no parameter, and is invoked after a commit is made.<br />This hook is meant primarily for notification, and cannot affect the outcome of !gitcommit.', _git_hooker_hook_description_args()),
  );
}
