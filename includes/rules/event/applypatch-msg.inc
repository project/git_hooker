<?php

/**
 * @file
 * Rules event
 */

function _git_hooker_rules_event_applypatch_msg_info() {
  return array(
    'label' => t('Git hook - Apply patch message'),
    'help' => t('This hook is invoked by !gitam script. It takes a single parameter, the name of the file that holds the proposed commit log message. Exiting with non-zero status causes git am to abort before applying the patch.<br />The hook is allowed to edit the message file in place, and can be used to normalize the message into some project standard format (if the project has one). It can also be used to refuse the commit after inspecting the message file.<br />The default !applypatchmsg hook, when enabled, runs the !commitmsg hook, if the latter is enabled.', _git_hooker_hook_description_args()),
  );
}
