<?php

/**
 * @file
 * Rules event
 */

function _git_hooker_rules_event_pre_rebase_info() {
  return array(
    'label' => t('Git hook - Pre rebase'),
    'help' => t('This hook is called by git rebase and can be used to prevent a branch from getting rebased.', _git_hooker_hook_description_args()),
  );
}
