<?php

/**
 * @file
 * Rules event
 */

function _git_hooker_rules_event_pre_commit_info() {
  return array(
    'label' => t('Git hook - Pre commit'),
    'help' => t('This hook is invoked by !gitcommit, and can be bypassed with !noverify option. It takes no parameter, and is invoked before obtaining the proposed commit log message and making a commit. Exiting with non-zero status from this script causes the !gitcommit to abort.<br />The default !precommit hook, when enabled, catches introduction of lines with trailing whitespaces and aborts the commit when such a line is found.<br />All the !gitcommit hooks are invoked with the environment variable !giteditor if the command will not bring up an editor to modify the commit message.', _git_hooker_hook_description_args()),
  );
}
