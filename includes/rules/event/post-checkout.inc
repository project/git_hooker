<?php

/**
 * @file
 * Rules event
 */

function _git_hooker_rules_event_post_checkout_info() {
  return array(
    'label' => t('Git hook - Post checkout'),
    'help' => t('This hook is invoked when a !gitcheckout is run after having updated the worktree. The hook is given three parameters: the ref of the previous HEAD, the ref of the new HEAD (which may or may not have changed), and a flag indicating whether the checkout was a branch checkout (changing branches, flag=1) or a file checkout (retrieving a file from the index, flag=0). This hook cannot affect the outcome of !gitcheckout.<br />It is also run after !gitclone, unless the --no-checkout (-n) option is used. The first parameter given to the hook is the null-ref, the second the ref of the new HEAD and the flag is always 1.<br />This hook can be used to perform repository validity checks, auto-display differences from the previous HEAD if different, or set working dir metadata properties.', _git_hooker_hook_description_args()),
  );
}
