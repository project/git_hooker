<?php

/**
 * @file
 * Rules event
 */

function _git_hooker_rules_event_post_applypatch_info() {
  return array(
    'label' => t('Git hook - Post apply patch'),
    'help' => t('This hook is invoked by !gitam. It takes no parameter, and is invoked after the patch is applied and a commit is made.<br />This hook is meant primarily for notification, and cannot affect the outcome of !gitam.', _git_hooker_hook_description_args()),
  );
}