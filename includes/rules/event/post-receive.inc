<?php

/**
 * @file
 * Rules event
 */

function _git_hooker_rules_event_post_receive_info() {
  return array(
    'label' => t('Git hook - Post receive'),
    'help' => t('This hook is invoked by git-receive-pack on the remote repository, which happens when a git push is done on a local repository. It executes on the remote repository once after all the refs have been updated.<br />
This hook executes once for the receive operation. It takes no arguments, but gets the same information as the pre-receive hook does on its standard input.<br />
This hook does not affect the outcome of git-receive-pack, as it is called after the real work is done.<br />
This supersedes the post-update hook in that it gets both old and new values of all the refs in addition to their names.<br />
Both standard output and standard error output are forwarded to git send-pack on the other end, so you can simply echo messages for the user.<br />
The default post-receive hook is empty, but there is a sample script post-receive-email provided in the contrib/hooks directory in git distribution, which implements sending commit emails.', _git_hooker_hook_description_args()),
  );
}
