<?php

/**
 * @file
 * Rules event
 */

function _git_hooker_rules_event_pre_auto_gc_info() {
  return array(
    'label' => t('Git hook - Pre auto GC'),
    'help' => t('This hook is invoked by git gc --auto. It takes no parameter, and exiting with non-zero status from this script causes the git gc --auto to abort.', _git_hooker_hook_description_args()),
  );
}
