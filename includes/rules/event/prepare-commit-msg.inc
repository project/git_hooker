<?php

/**
 * @file
 * Rules event
 */

function _git_hooker_rules_event_prepare_commit_msg_info() {
  return array(
    'label' => t('Prepare commit message'),
    'help' => t('This hook is invoked by !gitcommit right after preparing the default log message, and before the editor is started.<br />
It takes one to three parameters. The first is the name of the file that contains the commit log message. The second is the source of the commit message, and can be: message (if a -m or -F option was given); template (if a -t option was given or the configuration option commit.template is set); merge (if the commit is a merge or a .git/MERGE_MSG file exists); squash (if a .git/SQUASH_MSG file exists); or commit, followed by a commit SHA1 (if a -c, -C or --amend option was given).<br />
If the exit status is non-zero, git commit will abort.<br />The purpose of the hook is to edit the message file in place, and it is not suppressed by the --no-verify option. A non-zero exit means a failure of the hook and aborts the commit. It should not be used as replacement for pre-commit hook.<br />The sample prepare-commit-msg hook that comes with git comments out the Conflicts: part of a merge’s commit message.', _git_hooker_hook_description_args()),
  );
}
