<?php

/**
 * @file
 * Rules event
 */

function _git_hooker_rules_event_pre_applypatch_info() {
  return array(
    'label' => t('Git hook - Pre apply patch'),
    'help' => t('This hook is invoked by !gitam. It takes no parameter, and is invoked after the patch is applied, but before a commit is made.<br />If it exits with non-zero status, then the working tree will not be committed after applying the patch.<br />It can be used to inspect the current working tree and refuse to make a commit if it does not pass certain test.<br />The default !preapplypatch hook, when enabled, runs the !precommit hook, if the latter is enabled.', _git_hooker_hook_description_args()),
  );
}
