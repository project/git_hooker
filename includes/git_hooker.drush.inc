<?php

/**
 * @file
 * Drush integration
 */

/**
 * Implements hook_drush_help().
 */
function git_hooker_drush_help($section) {
  drush_print_r(func_get_args());
  switch ($section) {
    case 'drush:git-hooker':
      return dt('Git Hooker related commands');
  }
}

/**
 * Implements hook_drush_command().
 */
function git_hooker_drush_command() {
  $return = array();

  $return['git-hooker-test'] = array(
    'description' => dt('Developer helper.'),
  );

  $return['git-hooker-events'] = array(
    'description' => dt('List available Git hook events.'),
  );

  $return['git-hooker-invoke'] = array(
    'description' => dt('Invokes configured rules for the given Git hook event.'),
    'arguments' => array(
      'hook' => dt('Description'),
      'path' => dt('Path to Git repository'),
    ),
  );

  $return['git-hooker-rules-config-export'] = array(
    'description' => dt('Export Rules configurations'),
    'arguments' => array(
      'module' => dt('Module name'),
    ),
  );

  $return['git-hooker-texttable'] = array(
    'description' => dt('Text table'),
    'arguments' => array(
      'texttable' => dt('Text table'),
    ),
  );

  return $return;
}

/**
 * Implements drush_COMMAND().
 */
function drush_git_hooker_events() {
  foreach (git_hooker_git_events() as $item) {
    $hook = $item['git_hooker']['drush']['hook'];
    drush_print($hook);
    $rows = array(
      0 => array(
        0 => '    ',
        1 => $item['label'] . "\n" . ((!empty($item['help'])) ?
          drush_html_to_text($item['help'])
          :
          dt('Help missing.')
        ),
      ),
    );
    drush_print_table($rows);
  }
}

// <editor-fold defaultstate="collapsed" desc="Invoke">
/**
 * Implements drush_COMMAND_validate().
 */
function drush_git_hooker_invoke_validate($hook = NULL, $path = NULL) {
  if (empty($hook)) {
    drush_set_error('git_hooker_argument_missing', dt('Hook argument is required'));
    return;
  }

  if (empty($path)) {
    drush_set_error('git_hooker_argument_missing', dt('Path argument is required'));
    return;
  }

  if (!is_dir($path)) {
    drush_set_error('git_hooker_dir_not_exists', dt('Directory is not exists. !path', array('!path' => $path)));
    return;
  }

  $hook = git_hooker_git_event($hook);
  if (!$hook) {
    drush_set_error('git_hooker_hook_not_exists', dt('Hook is not exists. !hook', array('!hook' => $hook)));
    return;
  }

  if (!drush_shell_cd_and_exec($path, 'git status')) {
    drush_set_error('git_hooker_not_repository', dt('Not a git repository. !path', array('!path' => $path)));
    return;
  }
}

/**
 * Implements drush_COMMAND().
 */
function drush_git_hooker_invoke($hook, $path) {
  $event = git_hooker_git_event($hook);
  rules_invoke_event_by_args($event['name'], array(
    'git_hooker_repository' => git_hooker_repository_load($path),
  ));
  //drush_set_error('Test error to prevent execute the Git command');
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Rules config export">
/**
 * Implements hook_COMMAND_validate().
 */
function drush_git_hooker_rules_config_export_validate() {
  $configs = func_get_args();
  $module = array_shift($configs);
  if (!$module) {
    drush_set_error(dt('Module name is required'));
    return;
  }

  if (!module_exists($module)) {
    drush_set_error(dt('Module !module does not exists', array('!module' => $module)));
    return;
  }

  foreach ($configs as $config) {
    $filename = drupal_get_path('module', $module) . "/includes/rules/configuration/$config.inc";
    if (!is_file($filename)) {
      drush_set_error(dt('Configuration file does not exists.'));
      drush_print($filename);
    }
  }
}

/**
 * Implements hook_COMMAND().
 */
function drush_git_hooker_rules_config_export() {
  $configs = func_get_args();
  $module = array_shift($configs);

  $path = drupal_get_path('module', $module) . '/includes/rules/configuration';
  if (!$configs) {
    $configs = array_keys(file_scan_directory($path, '/.+\.inc$/', array('key' => 'name', 'recursive' => FALSE)));
  }

  $content_template = implode("\n", array(
    '<?php',
    '',
    '/**',
    ' * @file',
    ' * Rules configuration',
    ' */',
    '',
    "function _{$module}_rules_configuration_%{filename_safe}_info() {",
    '  return %{json};',
    '}',
    '',
  ));
  foreach ($configs as $filename) {
    $filename_safe = str_replace('-', '_', $filename);
    $config = rules_config_load("{$module}_{$filename_safe}");
    if (!$config) {
      drush_log(dt('Configuration !config is not exists.', array('!config' => $config)), 'error');
      continue;
    }

    $from_to = array(
      '%{filename_safe}' => $filename_safe,
      '%{json}' => var_export($config->export(), TRUE),
    );

    if (file_put_contents("$path/$filename.inc", strtr($content_template, $from_to))) {
      drush_log(dt('Configuration !config is exported successful.', array('!config' => $filename)), 'success');
    }
    else {
      drush_log(dt('Configuration !config export is failed.', array('!config' => $filename)), 'error');
    }
  }
}
// </editor-fold>

function drush_git_hooker_texttable_validate() {
  module_load_include('test', 'git_hooker');
  $args = func_get_args();
  $error = FALSE;

  if (!$args) {
    drush_set_error('git_hooker_missing_argument', dt('Missing test case name'));
    $error = TRUE;
  }

  if ($error) {
    $cases = git_hooker_test_texttable_cases();
    foreach(array_keys($cases) as $name) {
      drush_print($name);
    }
  }
}

function drush_git_hooker_texttable($prefix) {
  $cases = git_hooker_test_texttable_cases();
  drush_print('$i = 0;');
  foreach ($cases as $name => $case) {
    if (strpos($name, $prefix) !== 0) {
      continue;
    }
    $name = drupal_substr($name, drupal_strlen($prefix) + 1);
    drush_print("\$i = sprintf('%02d', ++\$i);");
    drush_print("\$cases[\$i]['expected'] = <<< TABLE");
    drush_print(git_hooker_test_texttable_case_output($case));
    drush_print('TABLE;');
    drush_print();
  }
}

/**
 * Implements drush_COMMAND().
 */
function drush_git_hooker_test() {
  //$path = '/home/andor/tmp/git_hook_tester';
  //$repo = git_hooker_repository_load($path);
  //drush_print_r($repo);

  $rows = array(
    // Row number
    array(
      'data' => array(
        // Cell number
        array(
          'colspan' => 3,
          'data' => 'filename',
        ),
      ),
    ),
    array(
      'data' => array(
        // Cell number
        array('data' => 'SeveriŐŰ'),
        array('data' => 'linenum'),
        array('data' => "Warning\nLine"),
      ),
    ),
    array(
      'data' => array(
        // Cell number
        array('data' => 'Severity'),
        array('data' => 'linenum', 'valign' => GitHookerTextTable::ALIGN_BOTTOM),
        array('data' => "Warning\nLine", 'align' => GitHookerTextTable::ALIGN_RIGHT),
      ),
    ),
    array(
      'data' => array(
        // Cell number
        array('data' => 'Footer'),
        array('data' => 'Footer', 'valign' => GitHookerTextTable::ALIGN_BOTTOM),
        array('data' => 'Footer', 'align' => GitHookerTextTable::ALIGN_RIGHT),
      ),
    ),
  );

  $table = new GitHookerTextTable();
  $table->setNumOfHeaderRows(1);
  $table->setNumOfFooterRows(1);
  $table->setPadding(1, array('padding_left', 'padding_right'));
  $table->setPadding(1, NULL, 'foot');
  $table->setRows($rows);
  drush_print($table->getOutput());
}


